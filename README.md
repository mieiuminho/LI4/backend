[hugo]: https://github.com/HugoCarvalho99
[hugo-pic]: https://github.com/HugoCarvalho99.png?size=120
[nelson]: https://github.com/nelsonmestevao
[nelson-pic]: https://github.com/nelsonmestevao.png?size=120
[pedro]: https://github.com/pedroribeiro22
[pedro-pic]: https://github.com/pedroribeiro22.png?size=120
[celso]: https://github.com/Basto97
[celso-pic]: https://github.com/Basto97.png?size=120
[ricardo]: https://github.com/ricardoslv
[ricardo-pic]: https://github.com/ricardoslv.png?size=120

<div align="center">
    <img src="src/main/resources/logo.png" alt="delegatewise" width="350px">
</div>

## :rocket: Getting Started

For data persistence this project uses a MongoDB database. You should have
MongoDB up and running.

For administrative help there is a
[backoffice](https://gitlab.com/mieiuminho/li4/backoffice) that provides logging
and statistical information on the app usage. For that reason, you need to have
MySQL running as well. The schema information for this relational database can
be found in `scripts/create_logs_database.sql`.

For image uploading a API key for [IMGBB](https://imgbb.com) is required.

This project uses settings configured in environment variables defined in the
`.env` file. Use the `.env.sample` as a starting point.

```bash
cp .env.sample .env
```

In order to get those properly exported is recommend to set up
[direnv](https://direnv.net/) for a terminal based work flow and the plugin
[EnvFile](https://github.com/Ashald/EnvFile) for IntelliJ.

The MongoBD database can be populated with fake information (used for testing
purposes) by running the following script. The password for the fake users is
**delegatewise**.

```bash
bin/populate
```

The data can also be exported from the database.

```bash
bin/export
```

### :inbox_tray: Prerequisites

The following software is required to be installed on your system:

- [Java SDK](https://openjdk.java.net/)
- [Maven](https://maven.apache.org/maven-features.html)

### :hammer: Development

Clean the repository.

```
bin/clean
```

Compile and start a server instance and visit <https://localhost:8080/>. The API
documentation is available at <https://localhost:8080/docs/>. You can change the
port if you want to.

```
bin/server [<port>]
```

Running tests.

```
bin/test
```

Test the API endpoints.

```
bin/test api
```

Format the code accordingly to common [guide lines](https://github.com/google/google-java-format).

```
bin/format
```

Lint your code with _checkstyle_.

```
bin/lint
```

Generate the javadoc documentation.

```
bin/docs
```

### :whale: Docker

If you want to setup the required databases using docker containers you can
easily do it with [docker-compose](https://docs.docker.com/compose/install/).

Create and start the containers.

```
docker-compose up
```

Start the previously created containers.

```
docker-compose start
```

Stop the containers.

```
docker-compose stop
```

Destroy the containers created.

```
docker-compose down
```

### :package: Deployment

Deploy the application to Heroku.

```
bin/deploy
```

## :busts_in_silhouette: Team

| [![Hugo][hugo-pic]][hugo] | [![Nelson][nelson-pic]][nelson] | [![Pedro][pedro-pic]][pedro] | [![Celso][celso-pic]][celso] | [![Ricardo][ricardo-pic]][ricardo] |
| :-----------------------: | :-----------------------------: | :--------------------------: | :--------------------------: | :--------------------------------: |
|   [Hugo Carvalho][hugo]   |    [Nelson Estevão][nelson]     |    [Pedro Ribeiro][pedro]    |   [Celso Rodrigues][celso]   |      [Ricardo Silva][ricardo]      |
