#!/bin/bash

function export() {
  local COLECTION="$1"
  local FILE="$2"

  mongoexport \
    --host "$DELEGATE_DATA_MONGODB_HOST" \
    --port "$DELEGATE_DATA_MONGODB_PORT" \
    --authenticationDatabase "admin" \
    -u "$DELEGATE_DATA_MONGODB_USERNAME" \
    -p "$DELEGATE_DATA_MONGODB_PASSWORD" \
    --db="$DELEGATE_DATA_MONGODB_DB" \
    --collection="$COLECTION" \
    --out="$FILE" --jsonArray

  clang-format -i "$FILE"

}

export users scripts/data/users.json
export workspaces scripts/data/workspaces.json
