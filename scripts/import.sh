#!/bin/bash

function import() {
  local COLECTION="$1"
  local FILE="$2"

  mongoimport \
    --host "$DELEGATE_DATA_MONGODB_HOST" \
    --port "$DELEGATE_DATA_MONGODB_PORT" \
    --authenticationDatabase "admin" \
    -u "$DELEGATE_DATA_MONGODB_USERNAME" \
    -p "$DELEGATE_DATA_MONGODB_PASSWORD" \
    --db="$DELEGATE_DATA_MONGODB_DB" \
    --collection="$COLECTION" \
    --file "$FILE" --jsonArray
}

import users scripts/data/users.json
import workspaces scripts/data/workspaces.json
