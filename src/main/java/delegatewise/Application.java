package delegatewise;

import delegatewise.database.Mongo;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;

@Import(Mongo.class)
@EnableScheduling
@SpringBootApplication
@SuppressWarnings("checkstyle:HideUtilityClassConstructor")
public class Application {
    private static Logger log = LogManager.getLogger(Application.class);
    private static final Level STATS = Level.forName("STATS", 350);

    @SuppressWarnings("checkstyle:FinalParameters")
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
