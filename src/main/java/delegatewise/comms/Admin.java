package delegatewise.comms;

public final class Admin {

    private String admin;

    public Admin(final String admin) {
        this.admin = admin;
    }

    public String getAdmin() {
        return this.admin;
    }

    public void setAdmin(final String admin) {
        this.admin = admin;
    }
}
