package delegatewise.comms;

@SuppressWarnings("checkstyle:LeftCurly")
public enum Frequency {
    DAILY,
    WEEKLY,
    ONE_OFF,
    TWO_X,
    THREE_X;

    public static Frequency getFrequency(final String arg) {

        Frequency r;

        switch (arg) {
            case "DAILY":
                {
                    r = DAILY;
                    break;
                }
            case "WEEKLY":
                {
                    r = WEEKLY;
                    break;
                }
            case "ONE_OFF":
                {
                    r = ONE_OFF;
                    break;
                }
            case "TWO_X":
                {
                    r = TWO_X;
                    break;
                }
            case "THREE_X":
                {
                    r = THREE_X;
                    break;
                }
            default:
                {
                    r = null;
                    break;
                }
        }
        return r;
    }
}
