package delegatewise.comms;

public final class JsonWebToken {

    private String jwt;

    /** Empty constructor. */
    public JsonWebToken() {}

    /**
     * Parameterized constructor.
     *
     * @param jwt The token that is the result of the login process.
     */
    public JsonWebToken(final String jwt) {
        this.jwt = jwt;
    }

    /**
     * Allows to get the string that contains the token.
     *
     * @return The string that contanis the token.
     */
    public String getJwt() {
        return this.jwt;
    }

    /**
     * Allows to set the JWT token.
     *
     * @param jwt The token that we want to set.
     */
    public void setJwt(final String jwt) {
        this.jwt = jwt;
    }
}
