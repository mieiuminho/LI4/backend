package delegatewise.comms;

public final class LogInForm {

    private String username;
    private String password;

    /** Empty constructor. */
    public LogInForm() {}

    public LogInForm(final String username, final String password) {
        this.username = username;
        this.password = password;
    }
    /**
     * Allows to get the username.
     *
     * @return The username.
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * Allows to set the username.
     *
     * @param username The username which we want to set.
     */
    public void setUsername(final String username) {
        this.username = username;
    }

    /**
     * Allows to get the password.
     *
     * @return The password.
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * Allows to set the password.
     *
     * @param password The password which we want to set.
     */
    public void setPassword(final String password) {
        this.password = password;
    }
}
