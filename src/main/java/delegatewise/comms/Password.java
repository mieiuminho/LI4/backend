package delegatewise.comms;

public final class Password {

    private String password;

    public Password() {};

    public void setPassword(final String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }
}
