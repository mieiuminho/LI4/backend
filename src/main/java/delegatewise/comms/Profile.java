package delegatewise.comms;

import delegatewise.model.User;

public final class Profile {

    private String username;
    private String name;
    private String email;
    private String avatar;
    private String token;

    /** Empty constructor. */
    public Profile() {}

    /**
     * Parameterized constructor.
     *
     * @param username The username we want to set.
     * @param name The name we want to set.
     * @param email The email we want to set.
     * @param avatar The avatar we want to set.
     * @param token JWT token
     */
    public Profile(
            final String username,
            final String name,
            final String email,
            final String avatar,
            final String token) {
        this.username = username;
        this.name = name;
        this.email = email;
        this.avatar = avatar;
        this.token = token;
    }

    /**
     * Parameterized constructor.
     *
     * @param u User from which we want to build the `Profile`.
     */
    public Profile(final User u) {
        this.username = u.getUsername();
        this.name = u.getName();
        this.email = u.getEmail();
        this.avatar = u.getAvatar();
    }

    /**
     * Allows to get the username.
     *
     * @return The username.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Allows to set the username.
     *
     * @param username The username we want to set.
     */
    public void setUsername(final String username) {
        this.username = username;
    }

    /**
     * Allows to get the name.
     *
     * @return The name.
     */
    public String getName() {
        return name;
    }

    /**
     * Allows to set the name.
     *
     * @param name The name we want to set.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Allows to get the email.
     *
     * @return The email.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Allows to set the email.
     *
     * @param email The email we want to set.
     */
    public void setEmail(final String email) {
        this.email = email;
    }

    /**
     * Allows to get the avatar.
     *
     * @return The avatar.
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     * Allows to set the avatar.
     *
     * @param avatar The avatar we want to set.
     */
    public void setAvatar(final String avatar) {
        this.avatar = avatar;
    }

    public String getToken() {
        return this.token;
    }

    public void setToken(final String token) {
        this.token = token;
    }
}
