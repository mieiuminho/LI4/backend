package delegatewise.comms;

public final class ReportId {

    private String reportId;

    public ReportId(final String reportId) {
        this.reportId = reportId;
    }

    public String getReportId() {
        return this.reportId;
    }

    public void setReportId(final String reportId) {
        this.reportId = reportId;
    }
}
