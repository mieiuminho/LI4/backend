package delegatewise.comms;

import delegatewise.model.Status;
import delegatewise.model.Task;

public final class SimplifiedTask {

    private String id;
    private String title;
    private String description;
    private String workspaceName;
    private String workspaceId;
    private Status status;

    /** Empty constructor. */
    public SimplifiedTask() {}

    /**
     * Parameterized constructor.
     *
     * @param t Task.
     * @param workspaceId Identifier of the workspace.
     * @param workspaceName Name of the workspace.
     */
    public SimplifiedTask(final Task t, final String workspaceId, final String workspaceName) {
        this.id = t.getId();
        this.title = t.getTitle();
        this.description = t.getDescription();
        this.workspaceName = workspaceName;
        this.workspaceId = workspaceId;
        this.status = t.getStatus();
    }

    /**
     * Allows to get identifier.
     *
     * @return The identifier.
     */
    public String getId() {
        return this.id;
    }

    /**
     * Allows to set the identifier.
     *
     * @param id The identifier we want to set.
     */
    public void setId(final String id) {
        this.id = id;
    }

    /**
     * Allows to get the title.
     *
     * @return The title.
     */
    public String getTitle() {
        return this.title;
    }

    /**
     * Allows to set the title.
     *
     * @param title The title we want to set.
     */
    public void setTitle(final String title) {
        this.title = title;
    }

    /**
     * Allows to get the description.
     *
     * @return The description.
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Allows to set the description.
     *
     * @param description The title we want to set.
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Allows to get the workspace identifier.
     *
     * @return The workspace identifier.
     */
    public String getWorkspaceName() {
        return this.workspaceName;
    }

    /**
     * Allows to set the workpsace identifier.
     *
     * @param workspaceName The workspace identifier we want to set.
     */
    public void setWorkspaceName(final String workspaceName) {
        this.workspaceName = workspaceName;
    }

    public String getWorkspaceId() {
        return this.workspaceId;
    }

    public void setWorkspaceId(final String workspaceId) {
        this.workspaceId = workspaceId;
    }

    /**
     * Allows to get the status.
     *
     * @return The status.
     */
    public Status getStatus() {
        return this.status;
    }

    /**
     * Allows to set the status.
     *
     * @param status The status that we want to set.
     */
    public void setStatus(final Status status) {
        this.status = status;
    }
}
