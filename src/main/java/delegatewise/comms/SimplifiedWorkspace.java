package delegatewise.comms;

import delegatewise.model.Workspace;

public final class SimplifiedWorkspace {

    private String id;
    private String name;
    private String image;

    /** Empty Constructor */
    public SimplifiedWorkspace() {}

    /**
     * Parameterized Constructor
     *
     * @param id The Workspace's id.
     * @param name The Workspace's name.
     * @param image The Workspace's icon.
     */
    public SimplifiedWorkspace(final String id, final String name, final String image) {
        this.id = id;
        this.name = name;
        this.image = image;
    }

    /**
     * Parameterized constructor.
     *
     * @param w The Workspace from which we want to build the `SimplifiedWorkspace`.
     */
    public SimplifiedWorkspace(final Workspace w) {
        this.id = w.getId();
        this.name = w.getName();
        this.image = w.getImage();
    }

    /**
     * Allows to get the id.
     *
     * @return The id of the workspace.
     */
    public String getID() {
        return this.id;
    }

    /**
     * Allows to get the name.
     *
     * @return The name.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Allows to get the icon.
     *
     * @return The icon.
     */
    public String getImage() {
        return this.image;
    }
}
