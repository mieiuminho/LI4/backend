package delegatewise.comms;

public final class Skill {

    private String skill;

    public Skill(final String skill) {
        this.skill = skill;
    }

    public String getSkill() {
        return this.skill;
    }

    public void setSkill(final String skill) {
        this.skill = skill;
    }
}
