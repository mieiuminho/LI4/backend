package delegatewise.comms;

public final class SkillForm {

    private String name;

    public SkillForm() {};

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }
}
