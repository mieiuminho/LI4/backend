package delegatewise.comms;

import delegatewise.model.Status;

public final class StatusJson {

    private Status status;

    public StatusJson(final Status status) {
        this.status = status;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(final Status status) {
        this.status = status;
    }
}
