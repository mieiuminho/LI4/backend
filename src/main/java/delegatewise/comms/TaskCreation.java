package delegatewise.comms;

@SuppressWarnings("checkstyle:ParameterNumber")
public final class TaskCreation {

    private String id;
    private String title;
    private String description;
    private String workspace;
    private double workload;
    private boolean flexible;
    private String skillset;
    private Frequency freq;

    /** Empty constructor. */
    public TaskCreation() {}

    /**
     * Parameterized constructor.
     *
     * @param id Task identifier.
     * @param title Title of the task.
     * @param description The description of the Task.
     * @param workspace The identifier of the Workspace to which the Task is associated.
     * @param workload The workload that the Task represents.
     * @param flexible The flexibility of the Task (can/cannot be performed past the Expiration
     *     Date).
     * @param skillset The skillset that is required in order to perform the Task.
     * @param freq Frequency of the task.
     */
    public TaskCreation(
            final String id,
            final String title,
            final String description,
            final String workspace,
            final double workload,
            final boolean flexible,
            final String skillset,
            final String freq) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.workspace = workspace;
        this.workload = workload;
        this.flexible = flexible;
        this.skillset = skillset;
        this.freq = Frequency.getFrequency(freq);
    }

    /**
     * Allows to get the ID.
     *
     * @return The ID.
     */
    public String getId() {
        return this.id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    /**
     * Allows to get the title.
     *
     * @return The title.
     */
    public String getTitle() {
        return this.title;
    }

    /**
     * Allows to set the title.
     *
     * @param title The title we want to set.
     */
    public void setTitle(final String title) {
        this.title = title;
    }

    /**
     * Allows to get the description.
     *
     * @return The description.
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Allows to set the description.
     *
     * @param description The description we want to set.
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Allows to get the workspace identifier.
     *
     * @return The workspace identifier.
     */
    public String getWorkspace() {
        return this.workspace;
    }

    /**
     * Allows to set the identifier of the workspace.
     *
     * @param workspace The identifier which we want to set.
     */
    public void setWorkspace(final String workspace) {
        this.workspace = workspace;
    }

    /**
     * Allows to get the workload.
     *
     * @return The workload.
     */
    public double getWorkload() {
        return this.workload;
    }

    /**
     * Allows to set the workload.
     *
     * @param workload The workload we want to set.
     */
    public void setWorkload(final double workload) {
        this.workload = workload;
    }

    /**
     * Allows to know wether the Task is flexible on it's due date or not.
     *
     * @return `True` if the Task is flexible on it's due date of `False` otherwise.
     */
    public boolean isFlexible() {
        return this.flexible;
    }

    /**
     * Allows to set the flexibility of the Task.
     *
     * @param flexible The flexibility we want to set.
     */
    public void setFlexible(final boolean flexible) {
        this.flexible = flexible;
    }

    /**
     * Allows to get the required skillset in order to perform the Task.
     *
     * @return The required skillset in order to perform the Task.
     */
    public String getSkillset() {
        return this.skillset;
    }

    /**
     * Allows to set the required skillset in order to perform the Task.
     *
     * @param skillset The skillset we want to set.
     */
    public void setSkillset(final String skillset) {
        this.skillset = skillset;
    }

    public Frequency getFreq() {
        return freq;
    }

    @Override
    public String toString() {
        return "TaskCreation{"
                + "id='"
                + id
                + '\''
                + ", title='"
                + title
                + '\''
                + ", description='"
                + description
                + '\''
                + ", workspace='"
                + workspace
                + '\''
                + ", workload="
                + workload
                + ", flexible="
                + flexible
                + ", skillset='"
                + skillset
                + '\''
                + ", freq="
                + freq
                + '}';
    }
}
