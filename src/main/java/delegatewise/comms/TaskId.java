package delegatewise.comms;

public final class TaskId {

    private String taskId;

    public TaskId(final String taskId) {
        this.taskId = taskId;
    }

    public String getTaskId() {
        return this.taskId;
    }

    public void setTaskId(final String taskId) {
        this.taskId = taskId;
    }
}
