package delegatewise.comms;

import delegatewise.utils.ImgBB;
import java.io.*;

public final class UserCreationForm {

    private String username;
    private String name;
    private String email;
    private String password;
    private String avatar;

    /** Empty Constructor */
    public UserCreationForm() {}

    /**
     * Allows to get the username.
     *
     * @return The username.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Allows to get the name.
     *
     * @return The name.
     */
    public String getName() {
        return name;
    }

    /**
     * Allows to get the email.
     *
     * @return The email.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Allows to get the password.
     *
     * @return The password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Allows to set the username.
     *
     * @param username The username we want to set.
     */
    public void setUsername(final String username) {
        this.username = username;
    }

    /**
     * Allows to set the name.
     *
     * @param name The name we want to set.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Allows to set the email.
     *
     * @param email The email we want to set.
     */
    public void setEmail(final String email) {
        this.email = email;
    }

    /**
     * Allows to set the password.
     *
     * @param password The password we want to set.
     */
    public void setPassword(final String password) {
        this.password = password;
    }

    /**
     * Allows to get the avatar.
     *
     * @return The avatar.
     */
    public String getAvatar() {
        return this.avatar;
    }

    /**
     * Allows to set the avatar.
     *
     * @param image The avatar we want to set.
     * @throws IOException In case the connection to the server isn't well established.
     */
    public void setAvatar(final String image) throws IOException {
        this.avatar = ImgBB.uploadBase64Image(image);
    }
}
