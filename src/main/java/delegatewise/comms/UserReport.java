package delegatewise.comms;

import java.util.Date;

public final class UserReport {

    private String reporterId;
    private String reportedId;
    private String taskId;
    private Date taskDate;
    private String description;

    public UserReport() {}

    public String getReporterId() {
        return reporterId;
    }

    public void setReporterId(final String reporterId) {
        this.reporterId = reporterId;
    }

    public String getReportedId() {
        return reportedId;
    }

    public void setReportedId(final String reportedId) {
        this.reportedId = reportedId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(final String taskId) {
        this.taskId = taskId;
    }

    public Date getTaskDate() {
        return taskDate;
    }

    public void setTaskDate(final Date taskDate) {
        this.taskDate = taskDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "UserReport{"
                + "reporterId='"
                + reporterId
                + '\''
                + ", reportedId='"
                + reportedId
                + '\''
                + ", taskId='"
                + taskId
                + '\''
                + ", taskDate="
                + taskDate
                + ", description='"
                + description
                + '\''
                + '}';
    }
}
