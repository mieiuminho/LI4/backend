package delegatewise.comms;

import delegatewise.utils.ImgBB;
import java.io.IOException;

public final class UserUpdateForm {

    private String username;
    private String name;
    private String email;
    private String password;
    private String avatar;
    private String currentPassword;

    /** Empty Constructor */
    public UserUpdateForm() {}

    /**
     * Allows to get the name.
     *
     * @return The name.
     */
    public String getName() {
        return name;
    }

    /**
     * Allows to set the name.
     *
     * @param name The name we want to set.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Allows to get the username.
     *
     * @return The username.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Allows to set the username.
     *
     * @param username The username we want to set.
     */
    public void setUsername(final String username) {
        this.username = username;
    }

    /**
     * Allows to get the email.
     *
     * @return The email.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Allows to set the email.
     *
     * @param email The email we want to set.
     */
    public void setEmail(final String email) {
        this.email = email;
    }

    /**
     * Allows to get the password.
     *
     * @return The password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Allows to set the password.
     *
     * @param password The password we want to set.
     */
    public void setPassword(final String password) {
        this.password = password;
    }

    /**
     * Allows to get the avatar.
     *
     * @return The avatar.
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     * Allows to set the avatar.
     *
     * @param avatar Image in Base64.
     */
    public void setAvatar(final String avatar) {
        this.avatar = avatar;
    }

    public void uploadAvatar() throws IOException {
        this.avatar = ImgBB.uploadBase64Image(this.avatar);
    }

    public String getCurrentPassword() {
        return this.currentPassword;
    }

    public void setCurrentPassword(final String currentPassword) {
        this.currentPassword = currentPassword;
    }

    @Override
    public String toString() {
        return "UserUpdateForm{"
                + "username='"
                + username
                + '\''
                + ", name='"
                + name
                + '\''
                + ", email='"
                + email
                + '\''
                + ", password='"
                + password
                + '\''
                + ", avatar='"
                + avatar
                + '\''
                + ", currentPassword='"
                + currentPassword
                + '\''
                + '}';
    }
}
