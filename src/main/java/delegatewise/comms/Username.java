package delegatewise.comms;

public final class Username {

    private String username;

    public Username(final String username) {
        this.username = username;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }
}
