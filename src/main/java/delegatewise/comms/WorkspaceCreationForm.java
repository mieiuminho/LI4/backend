package delegatewise.comms;

import delegatewise.utils.ImgBB;
import java.io.IOException;

public final class WorkspaceCreationForm {

    private String name;
    private String adminId;
    private String image;

    /** Empty Constructor */
    public WorkspaceCreationForm() {}

    /**
     * Allows to get the name.
     *
     * @return The name.
     */
    public String getName() {
        return name;
    }

    /**
     * Allows to set the name.
     *
     * @param name The name we want to set.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Allows to get the identifier of the Workspace's administrator.
     *
     * @return The identifier of the Workspace's administrator.
     */
    public String getAdminId() {
        return adminId;
    }

    /**
     * Allows to set the identifier of the Workspace's administrator.
     *
     * @param adminId The identifier we want to set.
     */
    public void setAdminId(final String adminId) {
        this.adminId = adminId;
    }

    /**
     * Allows to get the icon.
     *
     * @return The icon.
     */
    public String getImage() {
        return image;
    }

    /**
     * Allows to set the icon.
     *
     * @param image The icon we want to set.
     * @throws IOException In case the connection to the server isn't well established.
     */
    public void setImage(final String image) throws IOException {
        this.image = ImgBB.uploadBase64Image(image);
    }
}
