package delegatewise.comms;

public final class WorkspaceId {

    private String workspaceId;

    public WorkspaceId(final String workspaceId) {
        this.workspaceId = workspaceId;
    }

    public String getWorkspaceId() {
        return workspaceId;
    }

    public void setWorkspaceId(final String workspaceId) {
        this.workspaceId = workspaceId;
    }
}
