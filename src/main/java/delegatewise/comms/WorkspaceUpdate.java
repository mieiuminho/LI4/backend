package delegatewise.comms;

import delegatewise.utils.ImgBB;
import java.io.IOException;

public final class WorkspaceUpdate {

    private String name;
    private String image;

    /** Empty Constructor */
    public WorkspaceUpdate() {}

    /**
     * Allows to get the name.
     *
     * @return The name.
     */
    public String getName() {
        return name;
    }

    /**
     * Allows to set the name.
     *
     * @param name The name we want to set.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Allows to get the icon.
     *
     * @return The icon.
     */
    public String getImage() {
        return image;
    }

    /**
     * Allows to set the workspace image.
     *
     * @param image Image that we want to set in Base64.
     * @throws IOException In case someting goes wrong while uploading the image.
     */
    public void setImage(final String image) throws IOException {

        this.image = ImgBB.uploadBase64Image(image);
    }
}
