package delegatewise.config;

import delegatewise.utils.CronJobExecuter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@SuppressWarnings("checkstyle:DesignForExtension")
@Configuration
@EnableScheduling
public class AppConfig {

    @Bean
    public CronJobExecuter task() {
        return new CronJobExecuter();
    }
}
