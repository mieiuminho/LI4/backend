package delegatewise.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
public final class Index {

    private Logger log = LogManager.getLogger(Index.class);

    @GetMapping("/")
    public String index() {
        return "task sharing simplified";
    }
}
