package delegatewise.controller;

import delegatewise.comms.*;
import delegatewise.database.UsersRepository;
import delegatewise.database.WorkspacesRepository;
import delegatewise.exceptions.*;
import delegatewise.model.Task;
import delegatewise.model.User;
import delegatewise.model.Workspace;
import delegatewise.utils.Hash;
import delegatewise.utils.TaskScheduler;
import java.io.IOException;
import java.util.*;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
public final class MainController {
    private static Logger log = LogManager.getLogger(MainController.class);
    private static final Level STATS = Level.forName("STATS", 350);

    @Autowired private UsersRepository users;

    @Autowired private WorkspacesRepository workspaces;

    @GetMapping("/scheduler")
    public void execScheduler() {
        TaskScheduler t = new TaskScheduler(workspaces, users);
        t.schedule();
    }

    /**
     * Allows to add a workspace to the database.
     *
     * @param form Object that contains the necessary information to create the workspace.
     * @return The _Workspace_ object.
     * @throws AlreadyRegisteredWorkspace In case the user is already in workspace.
     * @throws AlreadyInWorkspaceException Same as above.
     * @throws ImageUploadException Error while uploading image.
     */
    @PostMapping("/workspaces")
    public Workspace addWorkspace(@RequestBody final WorkspaceCreationForm form)
            throws AlreadyRegisteredWorkspace, AlreadyInWorkspaceException, ImageUploadException {
        try {
            form.setImage(form.getImage());
        } catch (IOException e) {
            log.error("IMAGE UPLOAD ERROR: " + e.getMessage());
            throw new ImageUploadException();
        }
        if (!workspaces.existsByName(form.getName())) {

            String name = form.getName();
            User adminUser = this.users.findByUsername(form.getAdminId()).get();

            form.setAdminId(adminUser.getId());

            log.log(STATS, "New workspace {}", Hash.generate(name));

            this.workspaces.save(new Workspace(form));
            Workspace added = this.workspaces.findByName(name).get();
            String workspaceID = added.getId();

            adminUser.addToWorkspace(workspaceID);
            this.users.save(adminUser);

            return added;

        } else {

            throw new AlreadyRegisteredWorkspace();
        }
    }

    /**
     * Allows to accept a workspace invite to a certain user.
     *
     * @param username Username of the user that was invited.
     * @param workspace Workspace to which the user is invited.
     * @throws InexistentUser The user isn't registered.
     * @throws InexistentInvitation The invite does not exist.
     * @throws AlreadyInWorkspaceException The user is already in the workspace.
     */
    @PutMapping("/users/{username}/invites/{workspace}")
    public void acceptWorkspaceInvite(
            @PathVariable("username") final String username,
            @PathVariable("workspace") final String workspace)
            throws InexistentUser, InexistentInvitation, AlreadyInWorkspaceException {

        if (!this.users.existsByUsername(username)) {

            throw new InexistentUser();
        }

        User u = this.users.findByUsername(username).get();
        u.removeInvite(workspace);
        u.addToWorkspace(workspace);
        String userId = u.getId();

        Workspace w = this.workspaces.findById(workspace).get();
        w.addMember(userId);

        this.users.save(u);
        this.workspaces.save(w);
    }

    /**
     * Allos to get the list of the workspace in which a certain user is signed up.
     *
     * @param username Username of the user whose workspaces must be determined.
     * @return List of the workspace in which the user is signed up.
     * @throws InexistentUser The user is not registered.
     */
    @GetMapping("/users/{username}/workspaces")
    public List<SimplifiedWorkspace> getUsersWorkspaces(
            @PathVariable("username") final String username) throws InexistentUser {

        if (!this.users.existsByUsername(username)) {

            throw new InexistentUser();
        }

        List<SimplifiedWorkspace> workspacesList = new ArrayList<>();

        User u = this.users.findByUsername(username).get();

        Set<String> workspaceIds = u.getWorkspaces();

        workspaceIds.forEach(
                a ->
                        workspacesList.add(
                                new SimplifiedWorkspace(this.workspaces.findById(a).get())));

        return workspacesList;
    }

    /**
     * Allows to get the list of users of a certain workspace.
     *
     * @param workspaceId Identifier of the workspace whose users must be determined.
     * @return List of profiles os the signed up users.
     * @throws InexistentWorkspace The workspace does not exist.
     */
    @GetMapping("/workspaces/{workspace}/users")
    public List<Profile> getWorkspaceUsers(@PathVariable("workspace") final String workspaceId)
            throws InexistentWorkspace {
        if (!this.workspaces.existsById(workspaceId)) {
            throw new InexistentWorkspace();
        }
        List<String> userIds = this.workspaces.findById(workspaceId).get().getUsers();
        List<Profile> r = new ArrayList<>();
        for (String user : userIds) {
            if (this.users.existsById(user)) {
                User u = this.users.findById(user).get();
                Profile userProfile = new Profile(u);
                r.add(userProfile);
            }
        }
        return r;
    }

    /**
     * Allows to get the tasks of a certain user within a specified workspace.
     *
     * @param workspace Workspace identifier in which we want to know the tasks of the users.
     * @param userId Identifier of the user whose tasks we want to know.
     * @return The tasks of the user (indexed by identifier).
     * @throws InexistentWorkspace The workspace does not exist.
     * @throws NotInWorkspaceException The user isn't in the workspace.
     */
    public Map<String, List<SimplifiedTask>> getUserTasks(
            final String workspace, final String userId)
            throws InexistentWorkspace, NotInWorkspaceException {

        Map<String, List<SimplifiedTask>> r = new HashMap<>();
        List<String> weekdays =
                new ArrayList<>(
                        Arrays.asList(
                                "monday",
                                "tuesday",
                                "wednesday",
                                "thursday",
                                "friday",
                                "saturday"));
        weekdays.forEach(a -> r.put(a, new ArrayList<>()));

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        calendar.add(Calendar.DAY_OF_WEEK, -day + 1);
        Date sunday = calendar.getTime();
        calendar.add(Calendar.DAY_OF_WEEK, 6);
        Date saturday = calendar.getTime();

        if (!workspaces.existsById(workspace)) {

            throw new InexistentWorkspace();
        }

        try {

            Collection<Task> tasks = this.workspaces.findById(workspace).get().getUserTasks(userId);

            for (Task t : tasks) {
                calendar.setTime(t.getExpirationDate());
                Workspace w = this.workspaces.findById(t.getWorkspace()).get();
                String workspaceId = w.getId();
                String workspaceName = w.getName();
                if (t.getExpirationDate().after(sunday) && t.getExpirationDate().before(saturday)) {
                    int dayOfTheWeek = calendar.get(Calendar.DAY_OF_WEEK);
                    switch (dayOfTheWeek) {
                        case Calendar.MONDAY:
                            r.get(weekdays.get(0))
                                    .add(new SimplifiedTask(t, workspaceId, workspaceName));
                            break;
                        case Calendar.TUESDAY:
                            r.get(weekdays.get(1))
                                    .add(new SimplifiedTask(t, workspaceId, workspaceName));
                            break;
                        case Calendar.WEDNESDAY:
                            r.get(weekdays.get(2))
                                    .add(new SimplifiedTask(t, workspaceId, workspaceName));
                            break;
                        case Calendar.THURSDAY:
                            r.get(weekdays.get(3))
                                    .add(new SimplifiedTask(t, workspaceId, workspaceName));
                            break;
                        case Calendar.FRIDAY:
                            r.get(weekdays.get(4))
                                    .add(new SimplifiedTask(t, workspaceId, workspaceName));
                            break;
                        case Calendar.SATURDAY:
                            r.get(weekdays.get(5))
                                    .add(new SimplifiedTask(t, workspaceId, workspaceName));
                            break;
                        default:
                            break;
                    }
                }
            }

        } catch (NoSuchElementException | NotInWorkspaceException e) {
            log.error(e.getMessage(), e);
            throw e;
        }

        return r;
    }

    /**
     * Allows to get the user in a certain day (or, by default, for the whole week).
     *
     * @param username Username of the user whose schedule must be determined.
     * @param weekday Weekday in which we want the schedule.
     * @return List of the tasks of the user (indexed by weekday).
     * @throws InexistentWorkspace The workspace does not exist.
     * @throws InexistentUser The user is not registered.
     * @throws NotInWorkspaceException The user is not registered in the workspace.
     */
    @GetMapping("/schedule/{username}")
    public Map<String, List<SimplifiedTask>> getUsersSchedule(
            @PathVariable("username") final String username,
            @RequestParam(value = "weekday", defaultValue = "all") final String weekday)
            throws InexistentWorkspace, InexistentUser, NotInWorkspaceException {

        if (!this.users.existsByUsername(username)) {

            throw new InexistentUser();
        }
        Map<String, List<SimplifiedTask>> r = new HashMap<>();

        User u = this.users.findByUsername(username).get();

        Set<String> workspaceIds = u.getWorkspaces();

        if (weekday.equals("all")) {

            r.put("saturday", new ArrayList<>());
            r.put("monday", new ArrayList<>());
            r.put("tuesday", new ArrayList<>());
            r.put("wednesday", new ArrayList<>());
            r.put("thursday", new ArrayList<>());
            r.put("friday", new ArrayList<>());

            for (String id : workspaceIds) {

                Workspace w = this.workspaces.findById(id).get();

                Map<String, List<SimplifiedTask>> tasks = this.getUserTasks(w.getId(), u.getId());

                for (String day : tasks.keySet()) {

                    List<SimplifiedTask> myTs = tasks.get(day);

                    for (SimplifiedTask t : myTs) r.get(day).add(t);
                }
            }
        } else {

            r.put(weekday, new ArrayList<>());

            for (String id : workspaceIds) {

                Workspace w = this.workspaces.findById(id).get();

                Map<String, List<SimplifiedTask>> tasks = this.getUserTasks(w.getId(), u.getId());

                for (String day : tasks.keySet()) {

                    if (day.equals(weekday)) {

                        List<SimplifiedTask> myTs = tasks.get(day);

                        for (SimplifiedTask t : myTs) r.get(day).add(t);
                    }
                }
            }
        }

        return r;
    }

    /**
     * Allows to get the administrator of a given workspace.
     *
     * @param workspace Identifier of the workspace whose administrator must be determined.
     * @return JSON object that contains the username of the administrator.
     * @throws InexistentWorkspace The workspace does not exist.
     */
    @GetMapping("/workspaces/{workspace}/admin")
    public Admin getWorkspaceAdmin(@PathVariable("workspace") final String workspace)
            throws InexistentWorkspace {

        String adminUsername = null;

        if (!this.workspaces.existsById(workspace)) {
            throw new InexistentWorkspace();
        } else {

            String adminId = this.workspaces.findById(workspace).get().getAdminId();
            adminUsername = this.users.findById(adminId).get().getUsername();
        }

        return new Admin(adminUsername);
    }

    /**
     * Allows to get the skillset of a user within a given workspace.
     *
     * @param workspace Identifier of the workspace.
     * @param username Identifier of the user.
     * @return List of the skills that are associated with the user within the workspace.
     * @throws InexistentWorkspace The workspace does not exist.
     * @throws InexistentUser The user is not registered.
     */
    @GetMapping("/workspaces/{workspace}/skills/{username}")
    public Collection<String> getUserSkillWithinWorkspace(
            @PathVariable("workspace") final String workspace,
            @PathVariable("username") final String username)
            throws InexistentWorkspace, InexistentUser {

        Collection<String> skills = null;

        if (!this.users.existsByUsername(username)) {

            throw new InexistentUser();
        } else {

            Workspace ws = this.workspaces.findById(workspace).get();
            String userID = this.users.findByUsername(username).get().getId();
            if (ws.getUsers().contains(userID)) {
                skills = ws.getUserSkills(userID);
            } else {
                throw new CustomException(
                        "The user whose skills were requested is not registered in the workspace",
                        HttpStatus.BAD_REQUEST);
            }
        }

        return skills;
    }

    /**
     * Allows to sign a user to a workspace.
     *
     * @param workspace Identifier of the workspace.
     * @param username Username of the user we want to sign.
     * @throws InexistentUser The user does not exist.
     * @throws InexistentWorkspace The workspace does not exist.
     */
    @PostMapping("/workspaces/{workspace}/users/{username}")
    public void signUserInWorkspace(
            @PathVariable("workspace") final String workspace,
            @PathVariable("username") final String username)
            throws InexistentUser, InexistentWorkspace {

        if (!this.workspaces.existsById(workspace)) {

            throw new InexistentWorkspace();
        } else {

            if (!this.users.existsByUsername(username)) {

                throw new InexistentUser();
            } else {

                User u = this.users.findByUsername(username).get();
                try {
                    u.addToWorkspace(workspace);
                } catch (AlreadyInWorkspaceException e) {
                    log.error(e.getMessage(), e);
                } finally {
                    this.users.save(u);
                }

                Workspace w = this.workspaces.findById(workspace).get();
                try {
                    w.addMember(u.getId());
                } catch (AlreadyInWorkspaceException e) {
                    log.error(e.getMessage(), e);
                } finally {
                    this.workspaces.save(w);
                }
            }
        }
    }

    /**
     * Allows to remove a user from a workspace.
     *
     * @param workspace Identifier of the workspace.
     * @param username Username of the user we want to remove from the workspace.
     * @throws InexistentUser The user is not registered.
     * @throws InexistentWorkspace The workspace does not exist.
     * @throws NotInWorkspaceException The user is not registered in the workspace.
     */
    @DeleteMapping("/workspaces/{workspace}/users/{username}")
    public void removeUserFromWorkspace(
            @PathVariable("workspace") final String workspace,
            @PathVariable("username") final String username)
            throws InexistentUser, InexistentWorkspace, NotInWorkspaceException {

        if (!this.workspaces.existsById(workspace)) {

            throw new InexistentWorkspace();
        } else {

            if (!this.users.existsByUsername(username)) {

                throw new InexistentUser();
            } else {

                User u = this.users.findByUsername(username).get();
                try {
                    u.removeFromWorkspace(workspace);
                } catch (NotInWorkspaceException e) {
                    log.error(e.getMessage(), e);
                } finally {
                    this.users.save(u);
                }

                Workspace w = this.workspaces.findById(workspace).get();
                try {
                    w.removeMember(u.getId());
                } catch (NotInWorkspaceException e) {
                    log.error(e.getMessage(), e);
                    throw e;
                } finally {
                    this.workspaces.save(w);
                }
            }
        }
    }

    /**
     * Allows to dissociate a skill from a given user within a workspace.
     *
     * @param workspace Identifier of the workspace.
     * @param user Username of the user.
     * @param skill Skill that we want to dissociate from the user.
     * @return The skill that was removed.
     * @throws InexistentWorkspace The workspace does not exist.
     * @throws InexistentSkillException The skill does not exist.
     * @throws NotInWorkspaceException The user is not registered in the workspace.
     * @throws InexistentUser The user is not registered.
     */
    @DeleteMapping("/workspaces/{workspace}/users/{user}/skills")
    public Skill removeUserSkill(
            @PathVariable("workspace") final String workspace,
            @PathVariable("user") final String user,
            @RequestBody final SkillForm skill)
            throws InexistentWorkspace, InexistentSkillException, NotInWorkspaceException,
                    InexistentUser {
        if (!workspaces.existsById(workspace)) {

            throw new InexistentWorkspace();
        }

        if (!users.existsByUsername(user)) {

            throw new InexistentUser();
        }

        Workspace w = this.workspaces.findById(workspace).get();
        try {
            User u = this.users.findByUsername(user).get();
            w.removeUserSkill(u.getId(), skill.getName());
        } catch (NoSuchElementException | NotInWorkspaceException | InexistentSkillException e) {
            log.error(e.getMessage(), e);
            throw e;
        } finally {
            this.workspaces.save(w);
        }

        return new Skill(skill.getName());
    }

    /**
     * Allows to associate a skill to a user within a workspace.
     *
     * @param workspace Identifier of the workspace.
     * @param user Username of the user to which the skill must be associated.
     * @param skill Skill that must be associated with the user.
     * @throws InexistentWorkspace The workspace does not exist.
     * @throws DuplicateSkillException The skill already exists.
     * @throws NotInWorkspaceException The user is not registered in the workspace.
     * @throws InexistentSkillException The skill does not exist.
     */
    @PostMapping("/workspaces/{workspace}/users/{user}/skills")
    public void addUserSkill(
            @PathVariable("workspace") final String workspace,
            @PathVariable("user") final String user,
            @RequestBody final SkillForm skill)
            throws InexistentWorkspace, DuplicateSkillException, NotInWorkspaceException,
                    InexistentSkillException {
        if (!workspaces.existsById(workspace)) {

            throw new InexistentWorkspace();
        }

        Workspace w = this.workspaces.findById(workspace).get();
        try {
            User u = this.users.findByUsername(user).get();
            w.addUserSkill(u.getId(), skill.getName());
        } catch (NoSuchElementException
                | NotInWorkspaceException
                | DuplicateSkillException
                | InexistentSkillException e) {
            log.error(e.getMessage(), e);
            throw e;
        } finally {
            this.workspaces.save(w);
        }
    }

    /**
     * Allows to get the schedule of a given workspace.
     *
     * @param workspace Identifier of the workspace whose schedule must be determined.
     * @return List of tasks with the respective assignee (indexed by day).
     * @throws InexistentWorkspace The workspace does not exist.
     */
    @GetMapping("/workspaces/{workspace}/schedule")
    public Map<String, List<SimplifiedTaskWithAssignee>> getWorkspaceTasks(
            @PathVariable("workspace") final String workspace) throws InexistentWorkspace {

        Map<String, List<SimplifiedTaskWithAssignee>> r = new HashMap<>();

        List<String> weekdays =
                new ArrayList<>(
                        Arrays.asList(
                                "monday",
                                "tuesday",
                                "wednesday",
                                "thursday",
                                "friday",
                                "saturday"));
        weekdays.forEach(a -> r.put(a, new ArrayList<>()));

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        calendar.add(Calendar.DAY_OF_WEEK, -day + 1);
        Date sunday = calendar.getTime();
        calendar.add(Calendar.DAY_OF_WEEK, 6);
        Date saturday = calendar.getTime();

        if (!workspaces.existsById(workspace)) {

            throw new InexistentWorkspace();
        }

        try {

            Map<String, Task> wtasks = this.workspaces.findById(workspace).get().getTasks();

            Collection<Task> tasks = new ArrayList<>();

            for (String t : wtasks.keySet()) tasks.add(wtasks.get(t));

            for (Task t : tasks) {
                calendar.setTime(t.getExpirationDate());
                Workspace w = this.workspaces.findById(t.getWorkspace()).get();
                User assignee = this.users.findById(t.getAssignee()).get();
                String assigneeUsername = assignee.getUsername();
                String workspaceId = w.getId();
                if (t.getExpirationDate().after(sunday) && t.getExpirationDate().before(saturday)) {
                    int dayOfTheWeek = calendar.get(Calendar.DAY_OF_WEEK);
                    switch (dayOfTheWeek) {
                        case Calendar.MONDAY:
                            r.get(weekdays.get(0))
                                    .add(
                                            new SimplifiedTaskWithAssignee(
                                                    t, workspaceId, assigneeUsername));
                            break;
                        case Calendar.TUESDAY:
                            r.get(weekdays.get(1))
                                    .add(
                                            new SimplifiedTaskWithAssignee(
                                                    t, workspaceId, assigneeUsername));
                            break;
                        case Calendar.WEDNESDAY:
                            r.get(weekdays.get(2))
                                    .add(
                                            new SimplifiedTaskWithAssignee(
                                                    t, workspaceId, assigneeUsername));
                            break;
                        case Calendar.THURSDAY:
                            r.get(weekdays.get(3))
                                    .add(
                                            new SimplifiedTaskWithAssignee(
                                                    t, workspaceId, assigneeUsername));
                            break;
                        case Calendar.FRIDAY:
                            r.get(weekdays.get(4))
                                    .add(
                                            new SimplifiedTaskWithAssignee(
                                                    t, workspaceId, assigneeUsername));
                            break;
                        case Calendar.SATURDAY:
                            r.get(weekdays.get(5))
                                    .add(
                                            new SimplifiedTaskWithAssignee(
                                                    t, workspaceId, assigneeUsername));
                            break;
                        default:
                            break;
                    }
                }
            }

        } catch (NoSuchElementException e) {
            log.error(e.getMessage(), e);
            throw e;
        }

        return r;
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "This user is not registered")
    @ExceptionHandler(InexistentUser.class)
    public void inexistentUserHandler() {}

    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "This workspace is not registered")
    @ExceptionHandler(InexistentWorkspace.class)
    public void inexistentWorkspaceHandler() {}
}
