package delegatewise.controller;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import delegatewise.comms.*;
import delegatewise.database.UsersRepository;
import delegatewise.exceptions.*;
import delegatewise.model.User;
import delegatewise.security.JwtTokenProvider;
import delegatewise.security.PasswordTokenGenerator;
import delegatewise.utils.Hash;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
@CrossOrigin(origins = "*")
public final class UsersController {

    private static Logger log = LogManager.getLogger(UsersController.class);
    private static final Level STATS = Level.forName("STATS", 350);

    @Autowired private UsersRepository users;

    @Autowired private PasswordEncoder passwordEncoder;

    @Autowired private JwtTokenProvider jwtTokenProvider;

    @Autowired private AuthenticationManager authenticationManager;

    private PasswordTokenGenerator passwordTokens = new PasswordTokenGenerator();

    /**
     * Allows to get the all the users in the database.
     *
     * @return List of all the `User` objects in the database.
     */
    @GetMapping("")
    public List<Profile> getUsers() {
        List<Profile> profiles = new ArrayList<>();
        for (User u : this.users.findAll()) profiles.add(new Profile(u));
        return profiles;
    }

    /**
     * Allows to register a new user in the database (and get a login token).
     *
     * @param newUser Object that contains the necessary information to register a user.
     * @return `JsonWebToken` that can be used later to authenticate API requests.
     * @throws AlreadyRegisteredUsername
     * @throws ImageUploadException
     */
    @PostMapping("/signup")
    public JsonWebToken signup(@RequestBody final UserCreationForm newUser)
            throws AlreadyRegisteredUsername, ImageUploadException {
        try {
            newUser.setAvatar(newUser.getAvatar());
        } catch (IOException e) {
            log.error("IMAGE UPLOAD ERROR: " + e.getMessage());
            throw new ImageUploadException();
        }
        User user = new User(newUser);
        if (!users.existsByUsername(user.getUsername())) {
            user.setPassword(passwordEncoder.encode(user.getPassword() + user.getSalt()));
            users.save(user);
            log.log(STATS, "Registered account {}", Hash.generate(user.getUsername()));
            User registeredUser = this.users.findByUsername(user.getUsername()).get();
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            registeredUser.getUsername(),
                            newUser.getPassword() + registeredUser.getSalt()));
            return new JsonWebToken(
                    jwtTokenProvider.createToken(
                            registeredUser.getUsername(), registeredUser.getRoles()));
        } else {
            throw new AlreadyRegisteredUsername();
        }
    }

    /**
     * Allows to login and get the a token to authenticate further requests.
     *
     * @param login Object that contains the login information (Username and Password).
     * @return `JsonWebToken` that can be used later to authenticate API requests.
     * @throws InexistentUser
     * @throws BadCredentialsException
     */
    @PostMapping("/signin")
    public JsonWebToken login(@RequestBody final LogInForm login)
            throws InexistentUser, BadCredentialsException {
        if (!users.existsByUsername(login.getUsername())) {

            throw new InexistentUser();
        }

        User u = users.findByUsername(login.getUsername()).get();
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        login.getUsername(), login.getPassword() + u.getSalt()));
        log.log(STATS, "Login with account {}", Hash.generate(u.getUsername()));
        return new JsonWebToken(jwtTokenProvider.createToken(u.getUsername(), u.getRoles()));
    }

    /**
     * Allows to add a task to a given workspace.
     *
     * @param username Username of the user which we want to add the task.
     * @param workspaceId Identifier of the workspace to which the task must be added.
     * @param taskId Identifier of the task.
     * @throws InexistentUser
     * @throws NotInWorkspaceException
     */
    @PostMapping("/{username}/workspaces/{workspaceId}/tasks/{taskId}")
    public void addTask(
            @PathVariable("username") final String username,
            @PathVariable("workspaceId") final String workspaceId,
            @PathVariable("taskId") final String taskId)
            throws InexistentUser, NotInWorkspaceException {
        if (!users.existsByUsername(username)) {

            throw new InexistentUser();
        }

        User u = this.users.findByUsername(username).get();

        try {
            u.addTask(workspaceId, taskId);
        } catch (NotInWorkspaceException e) {
            log.error(e.getMessage(), e);
            throw e;
        } finally {
            this.users.save(u);
        }
    }

    /**
     * Allows to invite a user to a given workspace.
     *
     * @param username Username of the user that must be invited.
     * @param workspace Identifier of the workspace to which the user is invited.
     * @throws InexistentUser
     */
    @PostMapping("/{username}/invites/{workspace}")
    public void inviteToWorkspace(
            @PathVariable("username") final String username,
            @PathVariable("workspace") final String workspace)
            throws InexistentUser, CustomException {

        if (!users.existsByUsername(username)) {

            throw new InexistentUser();
        }

        User u = this.users.findByUsername(username).get();
        Set<String> userWorkspaces = u.getWorkspaces();
        if (userWorkspaces.contains(workspace)) {
            throw new CustomException(
                    "This user is already in the provided workspace", HttpStatus.BAD_REQUEST);
        }
        u.addInvite(workspace);
        this.users.save(u);
    }

    /**
     * Allows to refuse an invitation to join a workspace.
     *
     * @param username Username of the user that can remove the invitation.
     * @param workspace Identifier of the workspace whose invite must be removed.
     * @throws InexistentUser
     * @throws InexistentInvitation
     */
    @DeleteMapping("/{username}/invites/{workspace}")
    public void refuseInvitation(
            @PathVariable("username") final String username,
            @PathVariable("workspace") final String workspace)
            throws InexistentUser, InexistentInvitation {

        if (!this.users.existsByUsername(username)) {

            throw new InexistentUser();
        }

        User u = this.users.findByUsername(username).get();
        u.removeInvite(workspace);
        this.users.save(u);
    }

    /**
     * Allows to get all the workspace invites associated with a user.
     *
     * @param username Username of the user whose invites must be provided.
     * @return List of workspace identifiers to where the user has been invited.
     * @throws InexistentUser
     */
    @GetMapping("/{username}/invites")
    public List<String> getUserInvites(@PathVariable("username") final String username)
            throws InexistentUser {
        Set<String> invites;
        if (!users.existsByUsername(username)) {
            throw new InexistentUser();
        } else {
            User u = this.users.findByUsername(username).get();
            invites = u.getInvites();
        }

        return invites.stream().collect(Collectors.toList());
    }

    /**
     * Allows to get the profile of a given user.
     *
     * @param username Username of the user whose profile must be returned.
     * @return Profile of the given user.
     * @throws InexistentUser
     */
    @GetMapping("/{username}")
    public Profile search(@PathVariable final String username) throws InexistentUser {
        if (!users.existsByUsername(username)) {

            throw new InexistentUser();
        }

        User u = users.findByUsername(username).get();
        return new Profile(u);
    }

    /**
     * Allows to update the profile of a given user.
     *
     * @param username Username of the user whose profile must be updated.
     * @param ucf Object that contains the updated fields of the user.
     * @return The updated profile of the given user.
     * @throws InexistentUser
     * @throws ImageUploadException
     */
    @PutMapping("/{username}")
    public Profile updateUser(
            @PathVariable("username") final String username, @RequestBody final UserUpdateForm ucf)
            throws InexistentUser, ImageUploadException, BadCredentialsException {
        if (!users.existsByUsername(username)) {

            throw new InexistentUser();
        }

        try {
            if (ucf.getAvatar() != null) {
                ucf.uploadAvatar();
            }
        } catch (IOException e) {
            log.error("IMAGE UPLOAD ERROR: " + e.getMessage());
            throw new ImageUploadException();
        }
        User u = this.users.findByUsername(username).get();
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        u.getUsername(), ucf.getCurrentPassword() + u.getSalt()));

        String actualPassword = ucf.getCurrentPassword();
        if (ucf.getPassword() != null) {
            actualPassword = ucf.getPassword();
            ucf.setPassword(passwordEncoder.encode(ucf.getPassword() + u.getSalt()));
        }

        String newUsername = u.getUsername();

        if (ucf.getUsername() != null) {
            newUsername = ucf.getUsername();
        }

        u.update(ucf);

        this.users.save(u);

        User registeredUser = this.users.findByUsername(newUsername).get();

        JsonWebToken token = login(new LogInForm(newUsername, actualPassword));

        Profile p = new Profile(u);
        p.setToken(token.getJwt());
        return p;
    }

    /**
     * Allows to update the workload of a given user within a specified workspace.
     *
     * @param username Username of the user whose workload must be updated.
     * @param workspace Identifier of the workspace in which the user workload must be updated.
     * @param value Value with which the workload must be updated.
     * @throws InexistentUser
     * @throws NotInWorkspaceException
     */
    @PutMapping("/{username}/workspaces/{workspace}/{value}")
    public void updateWorkload(
            @PathVariable("username") final String username,
            @PathVariable("workspace") final String workspace,
            @PathVariable("value") final double value)
            throws InexistentUser, NotInWorkspaceException {
        if (!users.existsByUsername(username)) {

            throw new InexistentUser();
        }

        User u = this.users.findByUsername(username).get();

        try {
            u.updateWorkload(workspace, value);
        } catch (NotInWorkspaceException e) {
            log.error(e.getMessage(), e);
            throw e;
        } finally {
            this.users.save(u);
        }
    }

    /**
     * Allows to delete a user from the database.
     *
     * @param username Username of the user that must be deleted from the database.
     * @return JSON object that contains the username of the deleted user.
     * @throws InexistentUser
     */
    @DeleteMapping("/{username}")
    public Username delete(@PathVariable final String username) throws InexistentUser {

        if (!users.existsByUsername(username)) {

            throw new InexistentUser();
        }

        users.deleteByUsername(username);
        log.info("BO: DELETED ACCOUNT");
        log.log(STATS, "Deleted account {}", Hash.generate(username));
        return new Username(username);
    }

    /**
     * Allows to delete a specified task from a specified workspace.
     *
     * @param username Username of the user to whom the task is attributed.
     * @param workspace Identifier of the workspace in which the task is attributed.
     * @param taskId Identifier of the task that must be deleted.
     * @return JSON object that contains the identifier of the deleted task.
     * @throws InexistentUser
     * @throws NotInWorkspaceException
     * @throws InexistentTaskException
     */
    @DeleteMapping("/{username}/workspaces/{workspaceId}/tasks/{taskId}")
    public TaskId removeTask(
            @PathVariable("username") final String username,
            @PathVariable("workspace") final String workspace,
            @PathVariable("taskId") final String taskId)
            throws InexistentUser, NotInWorkspaceException, InexistentTaskException {
        if (!users.existsByUsername(username)) {

            throw new InexistentUser();
        }

        User u = this.users.findByUsername(username).get();

        try {
            u.removeTask(workspace, taskId);
        } catch (NotInWorkspaceException | InexistentTaskException e) {
            log.error(e.getMessage(), e);
            throw e;
        } finally {
            this.users.save(u);
        }

        return new TaskId(taskId);
    }

    @PostMapping("/{email}/recover")
    public void generatePasswordToken(@PathVariable("email") final String email)
            throws CustomException, InexistentUser {

        if (!users.existsByEmail(email)) {
            throw new InexistentUser();
        }

        User u = this.users.findByEmail(email).get();

        String token = passwordTokens.generateToken(u.getId());
        StringBuilder emailText = new StringBuilder();

        emailText.append("Hi " + u.getName() + ",\n\n");
        emailText.append(
                "If you requested a password reset for "
                        + u.getUsername()
                        + " click the link below. If you didn't make this request, simply ignore this email.\n");
        emailText.append("\thttps://delegatewise.com/app/reset/" + token + "\n");
        emailText.append("Take care! \n");

        try {
            HttpResponse<JsonNode> request =
                    Unirest.post(
                                    "https://api.mailgun.net/v3/"
                                            + System.getenv("MAILGUN_DOMAIN_NAME")
                                            + "/messages")
                            .basicAuth("api", System.getenv("MAILGUN_API_KEY"))
                            .queryString(
                                    "from",
                                    "delegatewise <verify@"
                                            + System.getenv("MAILGUN_DOMAIN_NAME")
                                            + ">")
                            .queryString("to", u.getEmail())
                            .queryString("subject", "Password Reset Request")
                            .queryString("text", emailText.toString())
                            .asJson();
        } catch (UnirestException e) {
            throw new CustomException(
                    "Something went wrong while sending the email",
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/recover/{token}")
    public JsonWebToken validateChangePasswordToken(
            @PathVariable("token") final String token, @RequestBody final Password passwordForm)
            throws CustomException {

        String userId = this.passwordTokens.toWhomBelongsToken(token);

        if (userId != null) {

            User u = this.users.findById(userId).get();
            boolean validToken = this.passwordTokens.isValid(token, userId);
            boolean isUsed = this.passwordTokens.isTokenUsed(token, userId);
            if (isUsed) {
                throw new CustomException(
                        "This token has already been used in order to reset a password",
                        HttpStatus.BAD_REQUEST);
            }
            if (validToken) {
                u.setPassword(passwordEncoder.encode(passwordForm.getPassword() + u.getSalt()));
                passwordTokens.setTokenUsed(token, userId);
                this.users.save(u);
                User userWithNewPassword = this.users.findById(userId).get();
                authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(
                                userWithNewPassword.getUsername(),
                                passwordForm.getPassword() + userWithNewPassword.getSalt()));
                return new JsonWebToken(
                        jwtTokenProvider.createToken(
                                userWithNewPassword.getUsername(), userWithNewPassword.getRoles()));
            } else {
                throw new CustomException("The token has expired", HttpStatus.BAD_REQUEST);
            }

        } else {

            throw new CustomException("The token is not valid", HttpStatus.BAD_REQUEST);
        }
    }

    @ResponseStatus(value = HttpStatus.CONFLICT, reason = "This username is already registered")
    @ExceptionHandler(AlreadyRegisteredUsername.class)
    public void alreadyRegisteredUsernameExceptionHandler() {}

    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "This user is not registered")
    @ExceptionHandler(InexistentUser.class)
    public void nonExistentUserExceptionHandler() {}

    @ResponseStatus(value = HttpStatus.FORBIDDEN, reason = "Incorrect password")
    @ExceptionHandler(BadCredentialsException.class)
    public void badCredentialsExceptionHandler() {}

    @ResponseStatus(
            value = HttpStatus.INTERNAL_SERVER_ERROR,
            reason = "Something went wrong while upload the image to the remote server")
    @ExceptionHandler(ImageUploadException.class)
    public void imageUploadExceptionHandler() {}

    public void cleanTasks() {
        for (User u : this.users.findAll()) {
            u.clearTasks();
        }
    }

    public void resetWorkload() {
        for (User u : this.users.findAll()) {
            u.resetWorkload();
        }
    }
}
