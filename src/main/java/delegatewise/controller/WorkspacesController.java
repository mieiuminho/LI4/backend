package delegatewise.controller;

import delegatewise.comms.*;
import delegatewise.database.WorkspacesRepository;
import delegatewise.exceptions.*;
import delegatewise.model.Report;
import delegatewise.model.Status;
import delegatewise.model.Workspace;
import delegatewise.utils.Hash;
import java.io.IOException;
import java.util.*;
import java.util.List;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/workspaces")
@CrossOrigin(origins = "*")
public final class WorkspacesController {

    private static Logger log = LogManager.getLogger(WorkspacesController.class);
    private static final Level STATS = Level.forName("STATS", 350);

    private int taskIdentifier = 0;
    private int taskCreationIdentifier = 0;
    private int reportIdentifier = 0;

    @Autowired private WorkspacesRepository workspaces;

    /**
     * Allows to add a task to a specified workspace.
     *
     * @param workspace Identifier of the workspace to which the task should be added.
     * @param protoTask Object that contains the necessary information to create the task.
     * @return Object that contains the task information.
     * @throws InexistentWorkspace
     * @throws DuplicateTaskException
     */
    @PostMapping("/{workspace}/tasks")
    public TaskCreation addTaskToWorkspace(
            @PathVariable("workspace") final String workspace,
            @RequestBody final TaskCreation protoTask)
            throws InexistentWorkspace, DuplicateTaskException {

        if (!workspaces.existsById(workspace)) {
            throw new InexistentWorkspace();
        }

        Workspace n = this.workspaces.findById(workspace).get();

        protoTask.setId(Integer.toString(this.taskCreationIdentifier++));

        try {
            // TODO: Verificar se é o admin que está a executar esta operação
            UUID uuid = UUID.randomUUID();
            protoTask.setId(uuid.toString());
            n.addTask(protoTask);
        } catch (DuplicateTaskException e) {
            log.error(e.getMessage(), e);
            throw e;
        } finally {
            this.workspaces.save(n);
        }

        return protoTask;
    }

    /**
     * Allows to update the workload of a specified user within a workspace.
     *
     * @param workspace Identifier of the workspace in which the workload should be updated.
     * @param user Identifier of the user whose workload should be updated.
     * @param value Value with which the workload should be updated.
     * @throws InexistentWorkspace
     * @throws NotInWorkspaceException
     */
    @PostMapping("/{workspace}/users/{user}/{value}")
    public void updateWorkload(
            @PathVariable("workspace") final String workspace,
            @PathVariable("user") final String user,
            @PathVariable("value") final Double value)
            throws InexistentWorkspace, NotInWorkspaceException {
        if (!this.workspaces.existsById(workspace)) {

            throw new InexistentWorkspace();
        }

        Workspace w = this.workspaces.findById(workspace).get();
        try {
            w.updateWorkload(user, value);
        } catch (NoSuchElementException | NotInWorkspaceException e) {
            log.error(e.getMessage(), e);
            throw e;
        } finally {
            this.workspaces.save(w);
        }
    }

    /**
     * Allows to get all the workspaces in the database.
     *
     * @return List of all the workspaces in the database.
     */
    @GetMapping
    public List<SimplifiedWorkspace> getWorkspaces() {
        List<SimplifiedWorkspace> r = new ArrayList<>();
        for (Workspace s : this.workspaces.findAll()) r.add(new SimplifiedWorkspace(s));
        return r;
    }

    /**
     * Allows to get a specified workspace.
     *
     * @param workspace Identifier of the workspace that should be returned.
     * @return Object that contains the workspace information.
     * @throws InexistentWorkspace
     */
    @GetMapping("/{workspace}")
    public SimplifiedWorkspace getWorkspace(@PathVariable("workspace") final String workspace)
            throws InexistentWorkspace {
        if (!this.workspaces.existsById(workspace)) {

            throw new InexistentWorkspace();
        }

        Workspace n = this.workspaces.findById(workspace).get();
        return new SimplifiedWorkspace(n.getId(), n.getName(), n.getImage());
    }

    /**
     * Allows to get the valued skills within a workspace.
     *
     * @param workspace Identifier of the workspace whose skills must be returned.
     * @return List of valued skills within the workspace.
     * @throws InexistentWorkspace
     */
    @GetMapping("/{workspace}/skills")
    public Collection<String> getWorkspaceSkills(@PathVariable("workspace") final String workspace)
            throws InexistentWorkspace {
        if (!workspaces.existsById(workspace)) {

            throw new InexistentWorkspace();
        }

        try {
            Workspace n = this.workspaces.findById(workspace).get();
            Collection<String> skills = n.getSkills();
            return skills;
        } catch (NoSuchElementException e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    /**
     * Allows to add a valued skill to a specified workspace.
     *
     * @param workspace Identifier of the workspace in which the skill should be added.
     * @param skill Skill that must be added.
     * @throws InexistentWorkspace
     * @throws DuplicateSkillException
     */
    @PostMapping("/{workspace}/skills")
    public void addSkillToWorkspace(
            @PathVariable("workspace") final String workspace, @RequestBody final SkillForm skill)
            throws InexistentWorkspace, DuplicateSkillException {
        if (!workspaces.existsById(workspace)) {
            throw new InexistentWorkspace();
        }

        Workspace w = this.workspaces.findById(workspace).get();

        try {
            w.addSkill(skill.getName());
        } catch (NoSuchElementException | DuplicateSkillException e) {
            log.error(e.getMessage(), e);
            throw e;
        } finally {
            this.workspaces.save(w);
        }
    }

    /**
     * Allows to remvoe a specified skill from a specified workspace.
     *
     * @param workspace Identifier of the workspace from which the skill must be removed.
     * @param skill Skill that must be removed.
     * @return JSON object that must be removed.
     * @throws InexistentWorkspace
     * @throws InexistentSkillException
     * @throws ReferencedSkillException
     */
    @DeleteMapping("/{workspace}/skills")
    public Skill removeSkillFromWorkspace(
            @PathVariable("workspace") final String workspace, @RequestBody final SkillForm skill)
            throws InexistentWorkspace, InexistentSkillException, ReferencedSkillException {
        if (!workspaces.existsById(workspace)) {
            throw new InexistentWorkspace();
        }

        Workspace w = this.workspaces.findById(workspace).get();

        try {
            w.removeSkill(skill.getName());
        } catch (ReferencedSkillException | NoSuchElementException | InexistentSkillException e) {
            log.error(e.getMessage(), e);
            throw e;
        } finally {
            this.workspaces.save(w);
        }

        return new Skill(skill.getName());
    }

    /**
     * Allows to get the all the tasks within a workspace.
     *
     * @param workspace Identifier of the workspace whose tasks must be determined.
     * @return List of the tasks within the workspace.
     * @throws InexistentWorkspace
     */
    @GetMapping("/{workspace}/tasks")
    public Collection<TaskCreation> getTasks(@PathVariable("workspace") final String workspace)
            throws InexistentWorkspace {
        if (!workspaces.existsById(workspace)) {

            throw new InexistentWorkspace();
        }
        try {
            Workspace n = this.workspaces.findById(workspace).get();
            return new ArrayList<>(n.getTaskForms());
        } catch (NoSuchElementException e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    /**
     * Allows to get a specified task within a workspace.
     *
     * @param workspace Identifier of the workspace.
     * @param taskId Identifier of the task that must be returned.
     * @return Object that contains the task information.
     * @throws InexistentWorkspace
     * @throws InexistentTaskException
     */
    @GetMapping("/{workspace}/tasks/{taskid}")
    public TaskCreation getTask(
            @PathVariable("workspace") final String workspace,
            @PathVariable("taskid") final String taskId)
            throws InexistentWorkspace, InexistentTaskException {
        if (!workspaces.existsById(workspace)) {

            throw new InexistentWorkspace();
        }
        try {
            Workspace ws = this.workspaces.findById(workspace).get();

            if (!ws.getTaskForms().contains(taskId)) {
                throw new InexistentTaskException();
            } else {
                ws.removeTask(taskId);
            }

            return ws.getTaskForm(taskId);

        } catch (NoSuchElementException e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    /**
     * Allows to get the status of a task.
     *
     * @param workspace Identifier of the workspace.
     * @param taskId Identifier of the task whose status must be returned.
     * @return JSON object that contains the status of the task.
     * @throws InexistentWorkspace
     * @throws NoSuchElementException
     * @throws InexistentTaskException
     * @throws TaskCompletionException
     */
    @PutMapping("/{workspace}/tasks/{taskId}/status")
    public StatusJson updateTaskStatus(
            @PathVariable("workspace") final String workspace,
            @PathVariable("taskId") final String taskId)
            throws InexistentWorkspace, NoSuchElementException, InexistentTaskException,
                    TaskCompletionException {
        Status s;

        if (!this.workspaces.existsById(workspace)) {

            throw new InexistentWorkspace();
        }

        Workspace w = this.workspaces.findById(workspace).get();
        try {
            s = w.updateTaskStatus(taskId);
            log.log(STATS, "Task completed");
        } catch (NoSuchElementException | InexistentTaskException | TaskCompletionException e) {
            log.error(e.getMessage(), e);
            throw e;
        } finally {
            this.workspaces.save(w);
        }

        return new StatusJson(s);
    }

    /**
     * Allows to update a specified task within a workspace.
     *
     * @param workspace Identifier of the workspace.
     * @param taskId Identifier of the workspace.
     * @param updated Object that contains the updates to the task.
     * @throws InexistentWorkspace
     * @throws InexistentTaskException
     */
    @PutMapping("/{workspace}/tasks/{taskId}")
    public void updateTaskFromWorkspace(
            @PathVariable("workspace") final String workspace,
            @PathVariable("taskId") final String taskId,
            @RequestBody final TaskCreation updated)
            throws InexistentWorkspace, InexistentTaskException {
        if (!workspaces.existsById(workspace)) {

            throw new InexistentWorkspace();
        }

        Workspace ws = this.workspaces.findById(workspace).get();

        try {

            if (ws.getTaskForm(taskId) == null) {
                throw new InexistentTaskException();
            }
            ws.updateTask(taskId, updated);
        } catch (NoSuchElementException e) {
            log.error(e.getMessage(), e);
            throw e;
        } finally {
            this.workspaces.save(ws);
        }
    }

    /**
     * Allows to update a specified workspace.
     *
     * @param workspace Identifier of the workspace that must be updated.
     * @param update Object that contains the updates to the workspace.
     * @return The updated workspace.
     * @throws InexistentWorkspace
     * @throws ImageUploadException
     */
    @PutMapping("/{workspace}")
    public Workspace updateWorkspace(
            @PathVariable("workspace") final String workspace,
            @RequestBody final WorkspaceUpdate update)
            throws InexistentWorkspace, ImageUploadException {
        if (!workspaces.existsById(workspace)) {

            throw new InexistentWorkspace();
        }

        try {
            update.setImage(update.getImage());
        } catch (IOException e) {
            log.error("IMAGE UPLOAD ERROR: " + e.getMessage());
            throw new ImageUploadException();
        }

        Workspace w = this.workspaces.findById(workspace).get();
        w.setName(update.getName());
        w.setImage(update.getImage());
        return this.workspaces.save(w);
    }

    /**
     * Allows to remove a specified workspace from the database.
     *
     * @param workspace Identifier of the workspace that must be removed from the database.
     * @return JSON object that contains the identifier of the removed workspace.
     * @throws InexistentWorkspace
     */
    @DeleteMapping("/{workspace}")
    public WorkspaceId removeWorkspace(@PathVariable("workspace") final String workspace)
            throws InexistentWorkspace {
        if (!workspaces.existsById(workspace)) {

            throw new InexistentWorkspace();
        }

        this.workspaces.deleteById(workspace);
        log.log(STATS, "Deleted workspace {}", Hash.generate(workspace));
        return new WorkspaceId(workspace);
    }

    /**
     * Allows to remove a specified task from a workspace.
     *
     * @param workspace Identifier of the workspace from which the task must be removed.
     * @param taskId Identifier of the task that must be removed.
     * @return JSON object that contains the identifier of the removed task.
     * @throws InexistentWorkspace
     * @throws InexistentTaskException
     */
    @DeleteMapping("/{workspace}/tasks/{taskId}")
    public TaskId removeTaskFromWorkspace(
            @PathVariable("workspace") final String workspace,
            @PathVariable("taskId") final String taskId)
            throws InexistentWorkspace, InexistentTaskException {
        if (!this.workspaces.existsById(workspace)) {

            throw new InexistentWorkspace();
        }

        Workspace w = this.workspaces.findById(workspace).get();
        try {
            w.removeTask(taskId);
        } catch (NoSuchElementException | InexistentTaskException e) {
            log.error(e.getMessage(), e);
            throw e;
        } finally {
            this.workspaces.save(w);
        }

        return new TaskId(taskId);
    }

    /** Allows to udapte the status of all tasks. */
    public void updateAllTaskStatus() {
        for (Workspace ws : this.workspaces.findAll()) {
            ws.updateStatus();
        }
    }

    /**
     * Allows to add a user report to a specified workspace.
     *
     * @param workspace Identifier of the workspace in which the report should be submitted.
     * @param ur Object that contains the report information.
     * @return JSON object that contains the identifier of the report.
     * @throws InexistentWorkspace
     */
    @PostMapping("{workspace}/report")
    public ReportId submitReport(
            @PathVariable("workspace") final String workspace, @RequestBody final UserReport ur)
            throws InexistentWorkspace {

        Report r = new Report(Integer.toString(reportIdentifier++), ur);

        if (!this.workspaces.existsById(workspace)) {
            throw new InexistentWorkspace();
        } else {
            Workspace w = this.workspaces.findById(workspace).get();
            w.submitReport(r);
            log.log(STATS, "Report submitted at workspace {}", Hash.generate(workspace));
            this.workspaces.save(w);
        }

        return new ReportId(r.getId());
    }

    /**
     * Allows to remove a specified report from a specified workspace.
     *
     * @param workspace Identifier of the workspace from which the report should be removed.
     * @param reportId Identifier of the report that should be deleted.
     * @return JSON object that contains the identifier of the removed report.
     * @throws InexistentWorkspace
     * @throws InexistentReport
     */
    @DeleteMapping("/{workspace}/report/{reportId}")
    public ReportId removeReport(
            @PathVariable("workspace") final String workspace,
            @PathVariable("reportId") final String reportId)
            throws InexistentWorkspace, InexistentReport {

        if (!this.workspaces.existsById(workspace)) {
            throw new InexistentWorkspace();

        } else {
            Workspace w = this.workspaces.findById(workspace).get();

            if (!w.getReports().containsKey(reportId)) {

                throw new InexistentReport();
            } else {
                w.removeReport(reportId);
                this.workspaces.save(w);
            }
        }

        return new ReportId(reportId);
    }

    /**
     * Allows to udapte a report in a specified workspace.
     *
     * @param workspace Identifier of the workspace in which the report should be updated.
     * @param reportId Identifier of the report that should be udapted.
     * @param ur Object that contains the updates to the report.
     * @return JSON object that contains the identifier of the updated report.
     * @throws InexistentWorkspace
     * @throws InexistentReport
     */
    @PutMapping("/{workspace}/report/{reportId}")
    public ReportId updateReport(
            @PathVariable("workspace") final String workspace,
            @PathVariable("reportId") final String reportId,
            @RequestBody final UserReport ur)
            throws InexistentWorkspace, InexistentReport {

        Report r = new Report(reportId, ur);

        if (!this.workspaces.existsById(workspace)) {
            throw new InexistentWorkspace();

        } else {
            Workspace w = this.workspaces.findById(workspace).get();

            if (!w.getReports().containsKey(reportId)) {

                throw new InexistentReport();
            } else {
                w.updateReport(reportId, r);
                this.workspaces.save(w);
            }
        }

        return new ReportId(reportId);
    }

    /**
     * Allows to get a specified report within a specified workspace.
     *
     * @param workspace Identifier of the workspace.
     * @param reportId Identifier of the report.
     * @return Object that contains the updated report information.
     * @throws InexistentWorkspace
     * @throws InexistentReport
     */
    @GetMapping("/{workspace}/report/{reportId}")
    public Report getReport(
            @PathVariable("workspace") final String workspace,
            @PathVariable("reportId") final String reportId)
            throws InexistentWorkspace, InexistentReport {

        Report r = null;
        if (!this.workspaces.existsById(workspace)) {

            throw new InexistentWorkspace();
        } else {

            Workspace w = this.workspaces.findById(workspace).get();
            if (!w.getReports().containsKey(reportId)) {

                throw new InexistentReport();
            } else {

                r = w.getReports().get(reportId);
            }
        }

        return r;
    }

    /**
     * Allows to get all the reports within a specified workspace.
     *
     * @param workspace Identifier of the workspace.
     * @return List of all the reports associated with the specified workspace.
     * @throws InexistentWorkspace List of all the reports associated with the specified workspace.
     */
    @GetMapping("/{workspace}/reports")
    public Collection<Report> getAllReports(@PathVariable("workspace") final String workspace)
            throws InexistentWorkspace {

        ArrayList<Report> r = null;
        if (!this.workspaces.existsById(workspace)) {

            throw new InexistentWorkspace();
        } else {

            Workspace w = this.workspaces.findById(workspace).get();
            Map<String, Report> reports = w.getReports();
            for (String key : reports.keySet()) {
                r.add(reports.get(key));
            }
        }

        return r;
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "This workspace isn't registered")
    @ExceptionHandler(InexistentWorkspace.class)
    public void inexistentWorkspaceExceptionHandler() {}

    @ResponseStatus(value = HttpStatus.CONFLICT, reason = "This workspace name is already taken")
    @ExceptionHandler(AlreadyRegisteredWorkspace.class)
    public void alreadyRegisteredWorkspace() {}

    @ResponseStatus(
            value = HttpStatus.INTERNAL_SERVER_ERROR,
            reason = "Something went wrong while upload the image to the remote server")
    @ExceptionHandler(ImageUploadException.class)
    public void imageUploadExceptionHandler() {}
}
