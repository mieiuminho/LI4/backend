package delegatewise.database;

import delegatewise.model.Workspace;
import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface WorkspacesRepository extends MongoRepository<Workspace, String> {

    Optional<Workspace> findById(String id);

    Optional<Workspace> findByName(String name);

    boolean existsById(String id);

    boolean existsByName(String name);

    void deleteById(String id);

    void deleteByName(String name);
}
