package delegatewise.exceptions;

public class AlreadyInWorkspaceException extends Exception {

    public AlreadyInWorkspaceException() {
        super("The user is already registered in this workspace");
    }
}
