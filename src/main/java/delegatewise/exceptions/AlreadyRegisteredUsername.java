package delegatewise.exceptions;

public class AlreadyRegisteredUsername extends Exception {

    public AlreadyRegisteredUsername() {
        super("This username is already associated with another user");
    }
}
