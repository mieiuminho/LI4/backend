package delegatewise.exceptions;

public class AlreadyRegisteredWorkspace extends Exception {

    public AlreadyRegisteredWorkspace() {
        super("This workspace is already registered");
    }
}
