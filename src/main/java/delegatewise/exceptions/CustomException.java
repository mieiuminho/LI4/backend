package delegatewise.exceptions;

import org.springframework.http.HttpStatus;

public final class CustomException extends RuntimeException {

    private final String message;
    private final HttpStatus httpStatus;

    public CustomException(final String messaage, final HttpStatus httpStatus) {
        this.message = messaage;
        this.httpStatus = httpStatus;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    public HttpStatus getHttpStatus() {
        return this.httpStatus;
    }
}
