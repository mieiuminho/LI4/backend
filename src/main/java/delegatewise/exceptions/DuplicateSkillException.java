package delegatewise.exceptions;

public class DuplicateSkillException extends Exception {
    public DuplicateSkillException() {
        super("This skill is already selected");
    }
}
