package delegatewise.exceptions;

public class DuplicateTaskException extends Exception {
    public DuplicateTaskException() {
        super("There's already a task with that ID");
    }
}
