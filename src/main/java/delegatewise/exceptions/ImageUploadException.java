package delegatewise.exceptions;

public class ImageUploadException extends Exception {

    public ImageUploadException() {
        super("An error occurred while trying to upload the image to the remote location");
    }
}
