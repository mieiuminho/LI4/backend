package delegatewise.exceptions;

public class InexistentInvitation extends Exception {

    public InexistentInvitation() {
        super("There's no invitation for this workspace");
    }
}
