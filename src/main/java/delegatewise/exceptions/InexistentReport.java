package delegatewise.exceptions;

public final class InexistentReport extends Exception {

    public InexistentReport() {
        super("There is no report with the provided identifier.");
    }
}
