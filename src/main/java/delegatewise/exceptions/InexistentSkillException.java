package delegatewise.exceptions;

public class InexistentSkillException extends Exception {
    public InexistentSkillException() {
        super("Skill is not recognized by this workspace");
    }
}
