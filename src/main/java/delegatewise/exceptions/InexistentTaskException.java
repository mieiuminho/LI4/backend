package delegatewise.exceptions;

public class InexistentTaskException extends Exception {

    public InexistentTaskException() {
        super("Inexistent Task");
    }
}
