package delegatewise.exceptions;

public class InexistentUser extends Exception {

    public InexistentUser() {
        super("This is user is not registered");
    }
}
