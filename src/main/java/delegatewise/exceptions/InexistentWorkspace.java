package delegatewise.exceptions;

public class InexistentWorkspace extends Exception {

    public InexistentWorkspace() {
        super("This workspace is not registered");
    }
}
