package delegatewise.exceptions;

public class NotInWorkspaceException extends Exception {

    public NotInWorkspaceException() {
        super("The user is not registered in this workspace");
    }
}
