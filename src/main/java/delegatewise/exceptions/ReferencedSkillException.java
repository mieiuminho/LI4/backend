package delegatewise.exceptions;

public class ReferencedSkillException extends Exception {

    public ReferencedSkillException() {
        super("The skill you tried to remove is referenced in a workspace task.");
    }
}
