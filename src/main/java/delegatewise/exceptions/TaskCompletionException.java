package delegatewise.exceptions;

@SuppressWarnings("checkstyle:LineLength")
public class TaskCompletionException extends Exception {
    public TaskCompletionException() {
        super(
                "Unable to complete task. This task may have expired, it may already have been completed or cannot be completed at this time");
    }
}
