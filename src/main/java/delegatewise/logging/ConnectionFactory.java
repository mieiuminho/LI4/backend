package delegatewise.logging;

import java.sql.Connection;
import java.sql.SQLException;
import org.apache.commons.dbcp2.BasicDataSource;

public final class ConnectionFactory {
    private static final String DB_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String DB_HOST = System.getenv("DELEGATE_DATA_MYSQL_HOST");
    private static final String DB_PORT = System.getenv("DELEGATE_DATA_MYSQL_PORT");
    private static final String DB_NAME = System.getenv("DELEGATE_DATA_MYSQL_DB");
    private static final String DB_USERNAME = System.getenv("DELEGATE_DATA_MYSQL_USERNAME");
    private static final String DB_PASSWORD = System.getenv("DELEGATE_DATA_MYSQL_PASSWORD");

    private static class Singleton {
        private static final ConnectionFactory INSTANCE = new ConnectionFactory();
    }

    private BasicDataSource dataSource = new BasicDataSource();

    private ConnectionFactory() {
        dataSource.setDriverClassName(DB_DRIVER);
        dataSource.setUrl("jdbc:mysql://" + DB_HOST + ":" + DB_PORT + "/" + DB_NAME);
        dataSource.setUsername(DB_USERNAME);
        dataSource.setPassword(DB_PASSWORD);
    }

    public static Connection getConnection() throws SQLException {
        return Singleton.INSTANCE.dataSource.getConnection();
    }
}
