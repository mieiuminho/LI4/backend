package delegatewise.model;

import delegatewise.comms.UserReport;
import java.time.Instant;
import java.util.Date;

public final class Report {

    private String id;
    private String reporterId;
    private String reportedId;
    private String taskId;
    private Date taskDate;
    private Date reportDate;
    private String description;

    public Report(final String id, final UserReport ur) {
        this.id = id;
        this.reporterId = ur.getReporterId();
        this.reportedId = ur.getReportedId();
        this.taskId = ur.getTaskId();
        this.taskDate = ur.getTaskDate();
        this.reportDate = Date.from(Instant.now());
        this.description = ur.getDescription();
    }

    public String getId() {
        return this.id;
    }

    public String getReportedId() {
        return reportedId;
    }

    public String getTaskId() {
        return taskId;
    }

    public Date getTaskDate() {
        return taskDate;
    }

    public Date getReportDate() {
        return reportDate;
    }

    public String getDescription() {
        return description;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public void setReportedId(final String reportedId) {
        this.reportedId = reportedId;
    }

    public void setTaskId(final String taskId) {
        this.taskId = taskId;
    }

    public void setTaskDate(final Date taskDate) {
        this.taskDate = taskDate;
    }

    public void setReportDate(final Date reportDate) {
        this.reportDate = reportDate;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Report{"
                + "reportedId='"
                + reportedId
                + '\''
                + ", taskId='"
                + taskId
                + '\''
                + ", taskDate="
                + taskDate
                + ", reportDate="
                + reportDate
                + ", description='"
                + description
                + '\''
                + '}';
    }
}
