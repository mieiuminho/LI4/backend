package delegatewise.model;

public enum Status {
    COMPLETED,
    FAILED,
    PENDING,
    LATE;

    public static Status getStatus(final String arg) {

        Status r;

        switch (arg) {
            case "COMPLETED":
                r = COMPLETED;
                break;
            case "FAILED":
                r = FAILED;
                break;
            case "PENDING":
                r = PENDING;
                break;
            case "LATE":
                r = LATE;
                break;
            default:
                r = null;
                break;
        }

        return r;
    }
}
