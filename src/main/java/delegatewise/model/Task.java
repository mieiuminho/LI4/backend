package delegatewise.model;

import static delegatewise.model.Status.*;

import delegatewise.comms.TaskCreation;
import java.util.Date;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public final class Task {

    private String id;
    private String title;
    private String description;

    private String assignee;
    private String workspace;

    private double workload;

    private Status status;
    private boolean flexible;
    private String skillSet;

    private Date expirationDate;

    /** Empty constructor. */
    public Task() {}

    /**
     * Parameterized constructor.
     *
     * @param id Identifier of the task.
     * @param title Title of the task.
     * @param description Description of the task.
     * @param assignee Assignee of the task.
     * @param workspaceId Workspace identifier.
     * @param workload Workload associated with the task.
     * @param flex Task's flexibility.
     * @param skillSet Necessary skillset to perform the task.
     * @param expirationDate Expiration date to perform the task.
     */
    @SuppressWarnings("checkstyle:ParameterNumber")
    public Task(
            final String id,
            final String title,
            final String description,
            final String assignee,
            final String workspaceId,
            final double workload,
            final boolean flex,
            final String skillSet,
            final Date expirationDate) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.assignee = assignee;
        this.workspace = workspaceId;
        this.workload = workload;
        this.status = PENDING;
        this.flexible = flex;
        this.skillSet = skillSet;
        this.expirationDate = expirationDate;
    }

    /**
     * Parameterized constructor.
     *
     * @param id Identifier of the task.
     * @param protoTask `TaskCreation` object.
     */
    public Task(final String id, final TaskCreation protoTask) {
        this.id = id;
        this.title = protoTask.getTitle();
        this.description = protoTask.getDescription();
        this.workspace = protoTask.getWorkspace();
        this.workload = protoTask.getWorkload();
        this.status = PENDING;
        this.flexible = protoTask.isFlexible();
        this.skillSet = protoTask.getSkillset();
    }
    /**
     * Copy constructor.
     *
     * @param oneTask `Task` object.
     */
    public Task(final Task oneTask) {
        this.id = oneTask.getId();
        this.title = oneTask.getTitle();
        this.description = oneTask.getDescription();
        this.assignee = oneTask.getAssignee();
        this.workspace = oneTask.getWorkspace();
        this.workload = oneTask.getWorkload();
        this.status = oneTask.getStatus();
        this.flexible = oneTask.isFlexible();
        this.skillSet = oneTask.getSkillSet();
        this.expirationDate = oneTask.getExpirationDate();
    }

    public boolean hasAssignee() {

        return (this.assignee != null);
    }

    /**
     * Allows to get the identifier.
     *
     * @return The identifier.
     */
    public String getId() {
        return id;
    }

    /**
     * Allows to set the identifier.
     *
     * @param id The identifier that we want to set.
     */
    public void setId(final String id) {
        this.id = id;
    }

    /**
     * Allows to get the title.
     *
     * @return The title.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Allows to set the title.
     *
     * @param title The title that we want to set.
     */
    public void setTitle(final String title) {
        this.title = title;
    }

    /**
     * Allows to get the description.
     *
     * @return The description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Allows to set the description.
     *
     * @param description The description that we want to set.
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Allows to get the assignee.
     *
     * @return The assignee.
     */
    public String getAssignee() {
        return assignee;
    }

    /**
     * Allows to set the assignee.
     *
     * @param assignee The assignee we want to set.
     */
    public void setAssignee(final String assignee) {
        this.assignee = assignee;
    }

    /**
     * Allows to get the workspace.
     *
     * @return The workpsace.
     */
    public String getWorkspace() {
        return workspace;
    }

    /**
     * Allows to set the workspace.
     *
     * @param workspace The workspace we want to set.
     */
    public void setWorkspace(final String workspace) {
        this.workspace = workspace;
    }

    /**
     * Allows to get the workload.
     *
     * @return The workload.
     */
    public double getWorkload() {
        return workload;
    }

    /**
     * Allows to set the workload.
     *
     * @param workload The workload we want to set.
     */
    public void setWorkload(final double workload) {
        this.workload = workload;
    }

    /**
     * Allows to get the status.
     *
     * @return The status.
     */
    public Status getStatus() {
        return status;
    }

    /**
     * Allows to set the status.
     *
     * @param status The status we want to set.
     */
    public void setStatus(final Status status) {
        this.status = status;
    }

    /**
     * Allows to know wether the task is completed.
     *
     * @return `True` if the task is completed or `False` otherwise.
     */
    public boolean isCompleted() {
        return this.status == COMPLETED;
    }

    /**
     * Allows to get the necessary skillset to perform the task.
     *
     * @return The necessary skillset to perform the task.
     */
    public String getSkillSet() {
        return skillSet;
    }

    /**
     * Allows to set the necesssary skillset to perform the task.
     *
     * @param skillSet The skillset we want to set.
     */
    public void setSkillSet(final String skillSet) {
        this.skillSet = skillSet;
    }

    /**
     * Allos to get the expirationd date.
     *
     * @return The expiration date.
     */
    public Date getExpirationDate() {
        return expirationDate;
    }

    /**
     * Allows to set the expiration date.
     *
     * @param expirationDate The expiration we want to set.
     */
    public void setExpirationDate(final Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    /**
     * Allows to know wether the task has expired or not.
     *
     * @return `True` if the task has expired or `False` otherwise.
     */
    public boolean hasExpired() {
        return this.expirationDate.after(new Date());
    }

    /**
     * Allows to know if the task is flexible.
     *
     * @return `True` if the task is flexible or `False` otherwise.
     */
    public boolean isFlexible() {
        return flexible;
    }

    /**
     * Allows to set the flexibility of the task.
     *
     * @param flexible Flexibility of the task.
     */
    public void setFlexible(final boolean flexible) {
        this.flexible = flexible;
    }

    /**
     * Clone constructor.
     *
     * @return New `Task` object.
     */
    public Task clone() {
        return new Task(this);
    }

    @Override
    public String toString() {
        return "Task{"
                + "id='"
                + id
                + '\''
                + ", title='"
                + title
                + '\''
                + ", description='"
                + description
                + '\''
                + ", assignee='"
                + assignee
                + '\''
                + ", workspace='"
                + workspace
                + '\''
                + ", workload="
                + workload
                + ", status="
                + status
                + ", flexible="
                + flexible
                + ", skillSet='"
                + skillSet
                + '\''
                + ", expirationDate="
                + expirationDate
                + '}';
    }
}
