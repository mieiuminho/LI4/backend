package delegatewise.model;

import delegatewise.comms.UserCreationForm;
import delegatewise.comms.UserUpdateForm;
import delegatewise.exceptions.AlreadyInWorkspaceException;
import delegatewise.exceptions.InexistentInvitation;
import delegatewise.exceptions.InexistentTaskException;
import delegatewise.exceptions.NotInWorkspaceException;
import java.util.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "users")
public final class User {

    @Id private String id;

    @NotNull private String username;

    private String name;

    @Email private String email;
    private String password;
    private String salt;
    private String avatar;

    private Set<String> invites;
    private Set<String> workSpaces;
    private Map<String, Collection<String>> tasks;

    private Map<String, Double> workload;
    private Map<String, Double> score;

    private List<Role> roles;

    /**
     * Allows to generate a `salt` with variable length.
     *
     * @param length Desired length for the `salt` string.
     * @return `salt` String.
     */
    @SuppressWarnings("checkstyle:MagicNumber")
    public static String getSalt(final int length) {
        StringBuilder salt = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            salt.append((char) (new Random().nextInt(26) + 'a'));
        }
        return new String(salt);
    }

    /** Empty constructor */
    public User() {}

    /**
     * Parameterized constructor.
     *
     * @param username The username of the user.
     * @param name The name of the user.
     * @param email The email of the user.
     * @param password The password of the user.
     * @param avatar The avatar of the user.
     */
    public User(
            final String username,
            final String name,
            final String email,
            final String password,
            final String avatar) {
        this.id = id;
        this.username = username;
        this.name = name;
        this.email = email;
        this.password = password;
        this.salt = User.getSalt(10);
        this.avatar = avatar;
        this.invites = new HashSet<>();
        this.workSpaces = new HashSet<>();
        this.tasks = new HashMap<>();
        this.workload = new HashMap<>();
        this.score = new HashMap<>();
        this.roles = new ArrayList<Role>(Arrays.asList(Role.ROLE_CLIENT));
    }

    /**
     * Parameterized constructor.
     *
     * @param user The `UserCreationForm` object from which we want to create the User.
     */
    public User(final UserCreationForm user) {
        this.username = user.getUsername();
        this.name = user.getName();
        this.email = user.getEmail();
        this.password = user.getPassword();
        this.salt = User.getSalt(10);
        this.avatar = user.getAvatar();
        this.invites = new HashSet<>();
        this.workSpaces = new HashSet<>();
        this.tasks = new HashMap<>();
        this.workload = new HashMap<>();
        this.score = new HashMap<>();
        this.roles = new ArrayList<>(Arrays.asList(Role.ROLE_CLIENT));
    }

    /**
     * Allows to get the identifier.
     *
     * @return The identifier.
     */
    public String getId() {
        return id;
    }

    /**
     * Allows to get the username.
     *
     * @return The username.
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * Allows to get the name.
     *
     * @return The name.
     */
    public String getName() {
        return name;
    }

    /**
     * Allows to get the name.
     *
     * @return The name.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Allows to get the password.
     *
     * @return The password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Allows to get the salt.
     *
     * @return The salt.
     */
    public String getSalt() {
        return salt;
    }

    /**
     * Allows to set the identifier.
     *
     * @param id The identifier we want to set.
     */
    public void setId(final String id) {
        this.id = id;
    }

    /**
     * Allows to set the username.
     *
     * @param username The username we want to set.
     */
    public void setUsername(final String username) {
        this.username = username;
    }

    /**
     * Allows to set the username.
     *
     * @param name The username we want to set.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Allows to set the email.
     *
     * @param email The email we want to set.
     */
    public void setEmail(final String email) {
        this.email = email;
    }

    /**
     * Allows to set the password.
     *
     * @param password The password we want to set.
     */
    public void setPassword(final String password) {
        this.password = password;
    }

    /**
     * Allows to set the salt.
     *
     * @param salt The salt we want to set.
     */
    public void setSalt(final String salt) {
        this.salt = salt;
    }

    /**
     * Allows to get the avatar.
     *
     * @return The avatar.
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     * Allows to set the avatar.
     *
     * @param avatar The avatar we want to set.
     */
    public void setAvatar(final String avatar) {
        this.avatar = avatar;
    }

    /**
     * Allows to get the workspace invites.
     *
     * @return Workspace invites.
     */
    public Set<String> getInvites() {
        return this.invites;
    }

    /**
     * Allows to set the invites.
     *
     * @param invites Invites we want to set.
     */
    public void setInvites(final Set<String> invites) {
        this.invites = invites;
    }
    /**
     * Allows to get the roles.
     *
     * @return The roles.
     */
    public List<Role> getRoles() {
        return this.roles;
    }

    /**
     * Allows to set the roles.
     *
     * @param roles The roles we want to set.
     */
    public void setRoles(final List<Role> roles) {
        this.roles = roles;
    }

    /**
     * Allows to get the workspaces of a user.
     *
     * @return Set of a users' workspaces.
     */
    public Set<String> getWorkspaces() {
        return this.workSpaces;
    }

    /**
     * Allows to set a users' workspaces.
     *
     * @param workspaces Workspaces we want to set.
     */
    public void setWorkspaces(final Set<String> workspaces) {
        this.workSpaces = workspaces;
    }

    /**
     * Allows to add a user to a workspace.
     *
     * @param workspaceId The workspace identifier.
     * @return "sucess" in case everything goes as planned (with no exceptions).
     * @throws AlreadyInWorkspaceException Thrown if the user is already registered in the
     *     workspace.
     */
    public String addToWorkspace(final String workspaceId) throws AlreadyInWorkspaceException {
        if (this.workSpaces.contains(workspaceId)) {

            throw new AlreadyInWorkspaceException();

        } else {

            this.workSpaces.add(workspaceId);
            this.tasks.put(workspaceId, new ArrayList<>());
            this.workload.put(workspaceId, 0.0);
            this.score.put(workspaceId, 0.0);

            return "success";
        }
    }

    /**
     * Allows to remove a user from a workspace.
     *
     * @param workspaceId Identifier of the workspace.
     * @throws NotInWorkspaceException Thrown if the user isn't registered in the workspace.
     */
    public void removeFromWorkspace(final String workspaceId) throws NotInWorkspaceException {
        if (!this.workSpaces.contains(workspaceId)) {
            throw new NotInWorkspaceException();
        } else {
            this.workSpaces.remove(workspaceId);
            this.tasks.remove(workspaceId);
            this.workload.remove(workspaceId);
            this.workload.remove(workspaceId);
        }
    }

    /**
     * Allows to assign a task to a user.
     *
     * @param workspaceId Workspace identifier.
     * @param taskId Task object.
     * @throws NotInWorkspaceException Thrown if the user isn't registered in the workspace passed
     *     as argument.
     */
    public void addTask(final String workspaceId, final String taskId)
            throws NotInWorkspaceException {
        if (!this.workSpaces.contains(workspaceId)) {

            throw new NotInWorkspaceException();

        } else {
            this.tasks.get(workspaceId).add(taskId);
        }
    }

    /**
     * Allows to dissociate a task from a user.
     *
     * @param workspaceId Workspace identifier.
     * @param taskId Task identifier.
     * @throws NotInWorkspaceException Thrown if the user isn't registered in the workspace passed
     *     as argument.
     * @throws InexistentTaskException Thrown if the task identifier passed as argument isn't
     *     associated to any task.
     */
    public void removeTask(final String workspaceId, final String taskId)
            throws NotInWorkspaceException, InexistentTaskException {
        if (!this.workSpaces.contains(workspaceId)) {
            throw new NotInWorkspaceException();
        }
        if (!this.tasks.get(workspaceId).contains(taskId)) {
            throw new InexistentTaskException();
        }

        this.tasks.get(workspaceId).remove(taskId);
    }
    /** Allows for the removal of tasks in bulk */
    public void clearTasks() {
        this.tasks.clear();
    }

    /**
     * Updates the workload of a user in a specified workspace.
     *
     * @param workspaceId Workspace identifier.
     * @param value Value with which the workload should be updated.
     * @throws NotInWorkspaceException Thrown if the user isn't registered in the specified
     *     workspace.
     */
    public void updateWorkload(final String workspaceId, final double value)
            throws NotInWorkspaceException {
        if (!this.workSpaces.contains(workspaceId)) {

            throw new NotInWorkspaceException();

        } else {

            double buf = this.score.get(workspaceId) + value;
            this.score.put(workspaceId, buf);
            buf = this.workload.get(workspaceId) + value;
            this.workload.put(workspaceId, buf);
        }
    }

    /** Allows the reset of the workload in all workspaces. */
    public void resetWorkload() {
        for (String s : this.workload.keySet()) {
            this.workload.put(s, 0.0);
        }
    }

    /**
     * Allows to update the user.
     *
     * @param ucf `UserUpdateForm` object that contains the update informations.
     */
    public void update(final UserUpdateForm ucf) {
        if (ucf.getName() != null) {
            this.name = ucf.getName();
        }
        if (ucf.getUsername() != null) {
            this.username = ucf.getUsername();
        }
        if (ucf.getEmail() != null) {
            this.email = ucf.getEmail();
        }
        if (ucf.getPassword() != null) {
            this.password = ucf.getPassword();
        }
        if (ucf.getAvatar() != null) {
            this.avatar = ucf.getAvatar();
        }
    }

    public void addInvite(final String workspace) {
        this.invites.add(workspace);
    }

    public void removeInvite(final String workspace) throws InexistentInvitation {
        if (!this.invites.contains(workspace)) {
            throw new InexistentInvitation();
        } else {
            this.invites.remove(workspace);
        }
    }

    public void cleanTaskAssignments() {

        List<String> keys = new ArrayList<>();

        for (String key : this.tasks.keySet()) {
            keys.add(key);
        }

        for (String key : keys) {
            this.tasks.remove(key);
        }

        for (String key : keys) {
            this.tasks.put(key, new ArrayList<>());
        }
    }
}
