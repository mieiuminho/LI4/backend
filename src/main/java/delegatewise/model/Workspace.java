package delegatewise.model;

import delegatewise.comms.Frequency;
import delegatewise.comms.TaskCreation;
import delegatewise.comms.WorkspaceCreationForm;
import delegatewise.exceptions.*;
import java.util.*;
import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "workspaces")
public final class Workspace {

    @Id private String id;

    @NotNull private String name;

    private String adminId;
    private String image;

    private Set<String> members;
    private Map<String, Task> tasks;
    private Map<String, TaskCreation> taskForms;
    private Set<String> skills;

    private Map<String, Collection<String>> userSkills;
    private Map<String, Double> userWorkloads;
    private Map<String, Double> userScores;
    private Map<String, Report> reports;

    /** Empty constructor */
    public Workspace() {}

    /**
     * Parameterized constructor.
     *
     * @param name Name of the workspace.
     * @param adminId Identifier of the administrator of the workspace.
     * @param image Icon of the workspace.
     */
    public Workspace(final String name, final String adminId, final String image) {
        this.name = name;
        this.adminId = adminId;
        this.image = image;
        this.members = new HashSet<>();
        this.members.add(adminId);
        this.tasks = new HashMap<>();
        this.taskForms = new HashMap<>();
        this.skills = new HashSet<>();
        this.userSkills = new HashMap<>();
        this.userSkills.put(adminId, new ArrayList<>());
        this.userWorkloads = new HashMap<>();
        this.userWorkloads.put(adminId, (double) 0);
        this.userScores = new HashMap<>();
        this.userScores.put(adminId, (double) 0);
        this.reports = new HashMap<>();
    }

    public Workspace(final WorkspaceCreationForm wscf) {
        this.name = wscf.getName();
        this.adminId = wscf.getAdminId();
        this.image = wscf.getImage();
        this.members = new HashSet<>();
        this.members.add(this.adminId);
        this.tasks = new HashMap<>();
        this.taskForms = new HashMap<>();
        this.skills = new HashSet<>();
        this.userSkills = new HashMap<>();
        this.userSkills.put(adminId, new ArrayList<>());
        this.userWorkloads = new HashMap<>();
        this.userWorkloads.put(adminId, (double) 0);
        this.userScores = new HashMap<>();
        this.userScores.put(adminId, (double) 0);
        this.reports = new HashMap<>();
    }

    /**
     * Allows to get the identifier of the workspace.
     *
     * @return The identifier of the workspace.
     */
    public String getId() {
        return this.id;
    }

    /**
     * Allows to set the workspace identifier.
     *
     * @param id Identifier of the workspace.
     */
    public void setId(final String id) {
        this.id = id;
    }

    /**
     * Allows to get the name.
     *
     * @return The name.
     */
    public String getName() {
        return name;
    }

    /**
     * Allows to set the name.
     *
     * @param name The name.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Allows to get the identifier of the administrator.
     *
     * @return The identifier of the administrator.
     */
    public String getAdminId() {
        return adminId;
    }

    /**
     * Allows to set the identifier of the administrator.
     *
     * @param adminId Identifier of the administrator we want to set.
     */
    public void setAdminId(final String adminId) {
        this.adminId = adminId;
    }

    /**
     * Allows to get the icon.
     *
     * @return The icon.
     */
    public String getImage() {
        return this.image;
    }

    /**
     * Allows to set the icon.
     *
     * @param image The icon we want to set.
     */
    public void setImage(final String image) {
        this.image = image;
    }

    public Map<String, Report> getReports() {
        return this.reports;
    }

    public void setReports(final Map<String, Report> reports) {
        this.reports = reports;
    }

    /**
     * Allows to add a member to a workspace.
     *
     * @param memberId Identifier of the member we want to add.
     * @throws AlreadyInWorkspaceException Thrown if the user is already registered in the
     *     workspace.
     */
    public void addMember(final String memberId) throws AlreadyInWorkspaceException {
        if (this.members.contains(memberId)) {
            throw new AlreadyInWorkspaceException();
        }
        this.members.add(memberId);
        this.userSkills.put(memberId, new HashSet<>());
        this.userWorkloads.put(memberId, 0.0);
        this.userScores.put(memberId, 0.0);
    }

    /**
     * Allows to remove a member from a workspace.
     *
     * @param memberId Identifier of the member we want to add.
     * @throws NotInWorkspaceException Thrown if the member is not registered in the workspace.
     */
    public void removeMember(final String memberId) throws NotInWorkspaceException {
        if (!this.members.contains(memberId)) {
            throw new NotInWorkspaceException();
        }
        this.members.remove(memberId);
        this.userSkills.remove(memberId);
        this.userWorkloads.remove(memberId);
        this.userScores.remove(memberId);
    }

    /**
     * Allows to add a skill to a workspace.
     *
     * @param skill Skill that we want to add.
     * @throws DuplicateSkillException Thrown if the skill is already registered in the workspace.
     */
    public void addSkill(final String skill) throws DuplicateSkillException {
        if (this.skills.contains(skill)) {
            throw new DuplicateSkillException();
        }
        this.skills.add(skill);
    }

    /**
     * Allows to remove a skill from a workspace.
     *
     * @param skill Skill that we want to remove.
     * @throws InexistentSkillException Thrown if the skill isn't registered in the workspace.
     * @throws ReferencedSkillException The skill you tried to remove is referenced in a task.
     */
    public void removeSkill(final String skill)
            throws InexistentSkillException, ReferencedSkillException {
        if (!this.skills.contains(skill)) {
            throw new InexistentSkillException();
        }

        for (String key : this.taskForms.keySet()) {
            TaskCreation t = this.taskForms.get(key);
            if (t.getSkillset().equals(skill)) {
                throw new ReferencedSkillException();
            }
        }

        this.skills.remove(skill);
    }

    /**
     * Allows to add a skill to a user within a workspace.
     *
     * @param memberId Identifier of the member.
     * @param skill Skill that we want to add to the member within the workspace.
     * @throws InexistentSkillException Thrown if the skill is not registered in the workspace.
     * @throws DuplicateSkillException Thrown if the skill is already associated with the user
     *     within the workspace.
     * @throws NotInWorkspaceException Thrown if the member isn't registered in the workspace.
     */
    public void addUserSkill(final String memberId, final String skill)
            throws InexistentSkillException, DuplicateSkillException, NotInWorkspaceException {
        if (!this.members.contains(memberId)) {
            throw new NotInWorkspaceException();
        }
        if (!this.skills.contains(skill)) {
            throw new InexistentSkillException();
        }
        if (this.userSkills.get(memberId).contains(skill)) {
            throw new DuplicateSkillException();
        }

        this.userSkills.get(memberId).add(skill);
    }

    /**
     * Allows to dissociate a skill from a user within a workspace.
     *
     * @param memberId Identifier of the member.
     * @param skill Skill that we want dissociate from the user.
     * @throws InexistentSkillException Thrown if the skill is not associate with the workspace.
     * @throws NotInWorkspaceException Thrown if the member is not registered in the workspace.
     */
    public void removeUserSkill(final String memberId, final String skill)
            throws InexistentSkillException, NotInWorkspaceException {
        if (!this.members.contains(memberId)) {
            throw new NotInWorkspaceException();
        }
        if (!this.skills.contains(skill)) {
            throw new InexistentSkillException();
        }
        if (!this.userSkills.get(memberId).contains(skill)) {
            throw new InexistentSkillException();
        }

        this.userSkills.get(memberId).remove(skill);
    }

    /**
     * Allows to add a task to a workspace.
     *
     * @param task `Task` object that we want to add.
     * @throws DuplicateTaskException Thrown if a task with the same Id already exsists in the
     *     workspace.
     */
    public void addTask(final TaskCreation task) throws DuplicateTaskException {

        if (this.taskForms.containsKey(task.getId())) {
            throw new DuplicateTaskException();
        }

        this.taskForms.put(task.getId(), task);
    }

    /**
     * Allows to remove a task from a workspace.
     *
     * @param taskId Identifier of the task we want to remove.
     * @throws InexistentTaskException Thrown if the task isn't associated with the workpsace.
     */
    public void removeTask(final String taskId) throws InexistentTaskException {
        if (!this.taskForms.containsKey(taskId)) {
            throw new InexistentTaskException();
        }
        this.taskForms.remove(taskId);
    }

    public void updateTask(final String taskId, final TaskCreation updated) {
        this.taskForms.put(taskId, updated);
    }

    /**
     * Allows to mark a task as completed.
     *
     * @param taskId Identifier of the task we want to mark as completed.
     * @throws InexistentTaskException Thrown if the task isn't associated with the workspace.
     * @throws TaskCompletionException
     */
    public Status updateTaskStatus(final String taskId)
            throws InexistentTaskException, TaskCompletionException {

        Map<Status, Status> possibleTransitions =
                Map.ofEntries(
                        Map.entry(Status.getStatus("PENDING"), Status.getStatus("COMPLETED")),
                        Map.entry(Status.getStatus("COMPLETED"), Status.getStatus("PENDING")),
                        Map.entry(Status.getStatus("LATE"), Status.getStatus("COMPLETED")));

        if (!this.tasks.containsKey(taskId)) {
            throw new InexistentTaskException();
        }

        Task t = this.tasks.get(taskId);

        Calendar c = Calendar.getInstance();
        int currentDay = c.get(Calendar.DAY_OF_WEEK);
        c.setTime(t.getExpirationDate());
        int taskDay = c.get(Calendar.DAY_OF_WEEK);

        if (!t.isFlexible() && currentDay != taskDay) {
            throw new TaskCompletionException();
        }

        Status newStatus = possibleTransitions.get(t.getStatus());
        this.tasks.get(taskId).setStatus(newStatus);

        if (newStatus == Status.PENDING) {
            newStatus = updateStatus(taskId);
        }

        return newStatus;
    }

    /**
     * Allows to update the workload of a user within a workspace.
     *
     * @param memberId Identifier of the member whose workload we want to update.
     * @param value Value with which the user workload should be updated.
     * @throws NotInWorkspaceException Thrown if the user isn't registered in the workpsace.
     */
    public void updateWorkload(final String memberId, final double value)
            throws NotInWorkspaceException {
        if (!this.members.contains(memberId)) {
            throw new NotInWorkspaceException();
        }
        double buf = this.userWorkloads.get(memberId) + value;
        this.userWorkloads.put(memberId, buf);
        buf = this.userScores.get(memberId) + value;
        this.userScores.put(memberId, buf);
    }

    /**
     * Allows to get a task provided an identifier.
     *
     * @param taskId The task identifier.
     * @return The `Task` object.
     */
    public Task getTask(final String taskId) {
        return this.tasks.get(taskId);
    }

    /**
     * Allows to the users that are registered in the workspace.
     *
     * @return List of the users that are registered in the workspace.
     */
    public List<String> getUsers() {
        return new ArrayList<>(this.members);
    }

    /**
     * Allows to get the skills associated with a workspace.
     *
     * @return List of the skills associated with the workspace.
     */
    public List<String> getSkills() {
        return new ArrayList<>(this.skills);
    }

    /**
     * Allows to get the identifiers of the tasks associated the workspace.
     *
     * @return List of the task identifiers.
     */
    public List<String> getTaksIds() {
        List<String> r = new ArrayList<>();
        this.tasks.values().forEach(task -> r.add(task.getId()));
        return r;
    }

    /**
     * Allows to get the tasks associated with a specifier user within a workspace.
     *
     * @param userId Identifier of the user whose tasks we want to get.
     * @return List of the tasks associated with the specifier user.
     * @throws NotInWorkspaceException Thrown if the specified user isn't registered in the
     *     workspace.
     */
    public List<Task> getUserTasks(final String userId) throws NotInWorkspaceException {
        List<Task> r = new ArrayList<>();
        for (Task t : this.tasks.values()) {
            if (t.hasAssignee() && t.getAssignee().equals(userId)) {
                r.add(t);
            }
        }
        return r;
    }

    @SuppressWarnings("checkstyle:LeftCurly")
    private int getWeekDayFromIndex(final int index) {
        switch (index) {
            case 0:
                {
                    return Calendar.MONDAY;
                }
            case 1:
                {
                    return Calendar.TUESDAY;
                }
            case 2:
                {
                    return Calendar.WEDNESDAY;
                }
            case 3:
                {
                    return Calendar.THURSDAY;
                }
            case 4:
                {
                    return Calendar.FRIDAY;
                }
            case 5:
                {
                    return Calendar.SATURDAY;
                }
            default:
                {
                    return Calendar.SUNDAY;
                }
        }
    }

    private Date getWeekDayDate(final int weekday) {
        Calendar c = Calendar.getInstance();
        int add = weekday - c.get(Calendar.DAY_OF_WEEK);
        c.add(Calendar.DAY_OF_WEEK, add);
        return c.getTime();
    }

    public void schedule() {
        int index, currentNum;
        List<Integer> numTasks = new ArrayList<>();

        for (int i = 0; i < 6; i++) {
            numTasks.add(i, 0);
        }

        List<String> toSpread = new ArrayList<>();
        Calendar now = Calendar.getInstance();

        for (Task t : this.tasks.values()) {

            now.setTime(t.getExpirationDate());
            if (now.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                toSpread.add(t.getId());
            } else {
                index = now.get(Calendar.DAY_OF_WEEK) - 2;
                currentNum = numTasks.get(index);
                numTasks.set(index, currentNum + 1);
            }
        }

        for (String str : toSpread) {
            index = numTasks.indexOf(Collections.min(numTasks));
            currentNum = numTasks.get(index);
            numTasks.set(index, currentNum + 1);
            int weekday = getWeekDayFromIndex(index);
            this.tasks.get(str).setExpirationDate(getWeekDayDate(weekday));
        }
    }

    public int generateTasks(final int controllerTaskIdentifier) {
        this.tasks.clear();
        List<String> toRemove = new ArrayList<>();
        Calendar c;
        int taskId = controllerTaskIdentifier;
        for (TaskCreation tc : this.taskForms.values()) {
            Frequency fbuf = tc.getFreq();
            if (fbuf == Frequency.ONE_OFF) {

                Task t = new Task(String.valueOf(taskId++), tc);
                t.setExpirationDate(getWeekDayDate(Calendar.SUNDAY));
                this.tasks.put(t.getId(), t);
                toRemove.add(tc.getId());

            } else if (fbuf == Frequency.WEEKLY) {

                Task t = new Task(String.valueOf(taskId++), tc);
                t.setExpirationDate(getWeekDayDate(Calendar.SUNDAY));
                this.tasks.put(t.getId(), t);

            } else if (fbuf == Frequency.TWO_X) {
                Task t1 = new Task(String.valueOf(taskId++), tc);
                t1.setExpirationDate(getWeekDayDate(Calendar.TUESDAY));

                Task t2 = new Task(String.valueOf(taskId++), tc);
                t2.setExpirationDate(getWeekDayDate(Calendar.THURSDAY));

                this.tasks.put(t1.getId(), t1);
                this.tasks.put(t2.getId(), t2);

            } else if (fbuf == Frequency.THREE_X) {

                Task t1 = new Task(String.valueOf(taskId++), tc);
                t1.setExpirationDate(getWeekDayDate(Calendar.MONDAY));

                Task t2 = new Task(String.valueOf(taskId++), tc);
                t2.setExpirationDate(getWeekDayDate(Calendar.WEDNESDAY));

                Task t3 = new Task(String.valueOf(taskId++), tc);
                t3.setExpirationDate(getWeekDayDate(Calendar.FRIDAY));

                this.tasks.put(t1.getId(), t1);
                this.tasks.put(t2.getId(), t2);
                this.tasks.put(t3.getId(), t3);
            } else if (fbuf == Frequency.DAILY) {

                Task t1 = new Task(String.valueOf(taskId++), tc);
                t1.setExpirationDate(getWeekDayDate(Calendar.MONDAY));

                Task t2 = new Task(String.valueOf(taskId++), tc);
                t2.setExpirationDate(getWeekDayDate(Calendar.TUESDAY));

                Task t3 = new Task(String.valueOf(taskId++), tc);
                t3.setExpirationDate(getWeekDayDate(Calendar.WEDNESDAY));

                Task t4 = new Task(String.valueOf(taskId++), tc);
                t4.setExpirationDate(getWeekDayDate(Calendar.THURSDAY));

                Task t5 = new Task(String.valueOf(taskId++), tc);
                t5.setExpirationDate(getWeekDayDate(Calendar.FRIDAY));

                Task t6 = new Task(String.valueOf(taskId++), tc);
                t6.setExpirationDate(getWeekDayDate(Calendar.SATURDAY));

                this.tasks.put(t1.getId(), t1);
                this.tasks.put(t2.getId(), t2);
                this.tasks.put(t3.getId(), t3);
                this.tasks.put(t4.getId(), t4);
                this.tasks.put(t5.getId(), t5);
                this.tasks.put(t6.getId(), t6);
            }
        }
        toRemove.forEach(a -> this.taskForms.remove(a));
        return taskId;
    }

    public void updateStatus() {
        Calendar now = Calendar.getInstance();
        Calendar expires = Calendar.getInstance();

        for (Task t : this.tasks.values()) {
            Status status = t.getStatus();

            if (status == Status.PENDING) {
                expires.setTime(t.getExpirationDate());

                if (expires.get(Calendar.DAY_OF_WEEK) < now.get(Calendar.DAY_OF_WEEK)) {
                    if (t.isFlexible()) t.setStatus(Status.LATE);
                    else t.setStatus(Status.FAILED);
                }
            }
        }
    }

    public Status updateStatus(final String taskId) {
        Status r = Status.PENDING;
        Calendar now = Calendar.getInstance();
        Calendar expires = Calendar.getInstance();
        Task t = this.tasks.get(taskId);

        if (t.getStatus() == Status.PENDING) {
            expires.setTime(t.getExpirationDate());

            if (expires.get(Calendar.DAY_OF_WEEK) < now.get(Calendar.DAY_OF_WEEK)) {
                if (t.isFlexible()) {
                    t.setStatus(Status.LATE);
                    r = Status.LATE;
                } else {
                    t.setStatus(Status.FAILED);
                    r = Status.FAILED;
                }
            }
        }
        return r;
    }

    public Map<String, Task> getTasks() {
        return this.tasks;
    }

    // Returns all Task Creation classes present in the workspace
    public Collection<TaskCreation> getTaskForms() {
        return this.taskForms.values();
    }

    // Returns a specific Task Creation present in the workspace
    public TaskCreation getTaskForm(final String task) {
        return this.taskForms.get(task);
    }

    // Returns the skills associated to a given user in the workspace
    public Collection<String> getUserSkills(final String userID) {

        return this.userSkills.get(userID);
    }

    public Map<String, TaskCreation> getCreationForms() {

        return this.taskForms;
    }

    public void submitReport(final Report r) {
        this.reports.put(r.getReportedId(), r);
    }

    public void removeReport(final String reportId) {
        if (!this.reports.containsKey(reportId)) {
            this.reports.remove(reportId);
        }
    }

    public void updateReport(final String reportId, final Report newReport) {
        if (!this.reports.containsKey(reportId)) {
            this.reports.put(reportId, newReport);
        }
    }

    public Report getReport(final String reportId) {
        if (!this.reports.containsKey(reportId)) {
            return this.reports.get(reportId);
        } else {
            return null;
        }
    }
}
