package delegatewise.security;

import java.security.SecureRandom;
import java.util.*;

public final class PasswordTokenGenerator {

    private final class Token {

        private String token;
        private Date created;
        private boolean used;

        private Token(final String token, final Date creation) {
            this.token = token;
            this.created = creation;
            this.used = false;
        }

        public String getToken() {
            return this.token;
        }

        public void setToken(final String token) {
            this.token = token;
        }

        public Date getDate() {
            return this.created;
        }

        public void setDate(final Date creation) {
            this.created = creation;
        }

        public boolean isUsed() {
            return this.used;
        }

        public void setUsed(final boolean used) {
            this.used = used;
        }
    }

    private Map<String, List<Token>> tokens;
    private long validityInSeconds = 600;

    public PasswordTokenGenerator() {
        this.tokens = new HashMap<>();
    }

    public boolean isTokenUsed(final String token, final String userIdentifier) {
        boolean r = false;
        List<Token> userTokens = this.tokens.get(userIdentifier);
        for (Token t : userTokens) {
            if (t.getToken().equals(token)) r = t.isUsed();
        }
        return r;
    }

    public void setTokenUsed(final String token, final String userIdentifier) {
        List<Token> userTokens = this.tokens.get(userIdentifier);
        for (Token t : userTokens) {
            if (t.getToken().equals(token)) t.setUsed(true);
        }
    }

    public String generateToken(final String userIdentifier) {
        byte[] array = new byte[6];
        SecureRandom random = new SecureRandom();
        random.nextBytes(array);
        UUID randomLimited = UUID.nameUUIDFromBytes(array);
        String generatedString = randomLimited.toString();
        this.tokens.computeIfAbsent(userIdentifier, k -> new ArrayList<>());
        this.tokens.get(userIdentifier).add(new Token(generatedString, new Date()));
        return generatedString;
    }

    public boolean isValid(final String token, final String userIdentifier) {

        if (this.tokens.containsKey(userIdentifier)) {

            List<Token> userTokens = this.tokens.get(userIdentifier);

            if (userTokens.contains(token)) {

                Token tkn = null;

                for (Token t : userTokens) if (t.getToken() == token) tkn = t;

                Date creationDate = tkn.getDate();
                Date now = new Date();

                long seconds = (now.getTime() - creationDate.getTime()) / 1000;

                if (seconds > this.validityInSeconds) {
                    return false;
                }
            }
        }
        return true;
    }

    public String toWhomBelongsToken(final String token) {

        String userId = null;

        for (String key : this.tokens.keySet()) {

            List<Token> userTokens = this.tokens.get(key);

            for (Token t : userTokens) {

                if (t.getToken().equals(token)) {
                    userId = key;
                }
            }
        }

        return userId;
    }
}
