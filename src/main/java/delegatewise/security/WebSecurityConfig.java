package delegatewise.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;

@SuppressWarnings("checkstyle:DesignForExtension")
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired private JwtTokenProvider jwtTokenProvider;

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http.cors()
                .configurationSource(
                        request -> {
                            CorsConfiguration config = new CorsConfiguration();
                            config.setAllowCredentials(true);
                            config.addAllowedOrigin("*");
                            config.addAllowedHeader("*");
                            config.addAllowedMethod("DELETE");
                            config.addAllowedMethod("GET");
                            config.addAllowedMethod("PUT");
                            config.addAllowedMethod("POST");
                            return config.applyPermitDefaultValues();
                        });

        http.csrf().disable();

        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.authorizeRequests()
                .antMatchers("/users/signin")
                .permitAll()
                .antMatchers("/users/signup")
                .permitAll()
                .antMatchers("/docs-api/**")
                .permitAll()
                .antMatchers("/docs-api")
                .permitAll()
                .antMatchers("/swagger-ui/**")
                .permitAll()
                .antMatchers("/swagger-ui")
                .permitAll()
                .antMatchers("/docs")
                .permitAll()
                .antMatchers("/")
                .permitAll()
                .antMatchers("/humans.txt")
                .permitAll()
                .antMatchers("/scheduler")
                .permitAll()
                .antMatchers("/users/*/recover")
                .permitAll()
                .antMatchers("/users/recover/*")
                .permitAll()
                .anyRequest()
                .authenticated();

        http.exceptionHandling().accessDeniedPage("/login");

        http.apply(new JwtTokenFilterConfigurer(jwtTokenProvider));
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(12);
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
