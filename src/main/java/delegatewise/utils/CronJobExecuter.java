package delegatewise.utils;

import delegatewise.database.UsersRepository;
import delegatewise.database.WorkspacesRepository;
import delegatewise.model.Workspace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

public final class CronJobExecuter {

    @Autowired private WorkspacesRepository workspaces;

    @Autowired private UsersRepository users;

    @Scheduled(cron = "0 0 12 * * SUN")
    public void execute() {

        TaskScheduler t = new TaskScheduler(this.workspaces, this.users);
        t.schedule();
    }

    @Scheduled(cron = "0 0 0 * * *")
    public void updateTasksStatus() {

        for (Workspace ws : this.workspaces.findAll()) {
            ws.updateStatus();
        }
    }
}
