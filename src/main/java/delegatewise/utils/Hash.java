package delegatewise.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

@SuppressWarnings("checkstyle:HideUtilityClassConstructor")
public final class Hash {

    public Hash() {}

    public static String generate(final String word) {

        try {
            byte[] hash = MessageDigest.getInstance("SHA-512").digest(word.getBytes());
            return Base64.getEncoder().encodeToString(hash);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }
}
