package delegatewise.utils;

import com.google.gson.JsonParser;
import java.io.IOException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import org.apache.http.util.EntityUtils;

public final class ImgBB {

    private static final String URL = "https://api.imgbb.com/1/upload?key=";
    private static final String UPLOAD_KEY = System.getenv("DELEGATE_IMGBB_API_KEY");

    private ImgBB() {}

    public static String uploadBase64Image(final String image) throws IOException {

        HttpResponse response =
                Request.Post(URL + UPLOAD_KEY)
                        .bodyForm(Form.form().add("image", image).build())
                        .execute()
                        .returnResponse();

        HttpEntity entity = response.getEntity();

        String responseString = EntityUtils.toString(entity, "UTF-8");

        String displayUrl =
                JsonParser.parseString(responseString)
                        .getAsJsonObject()
                        .get("data")
                        .getAsJsonObject()
                        .get("display_url")
                        .getAsString();

        return displayUrl;
    }
}
