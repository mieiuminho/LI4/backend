package delegatewise.utils;

import delegatewise.database.UsersRepository;
import delegatewise.database.WorkspacesRepository;
import delegatewise.model.User;
import delegatewise.model.Workspace;
import java.io.IOException;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Request;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public final class TaskScheduler {

    private static Logger log = LogManager.getLogger(TaskScheduler.class);

    private WorkspacesRepository workspaces;
    private UsersRepository users;

    private int taskIdentifier = 0;

    private static final String DELEGATOR = System.getenv("DELEGATE_DELEGATOR_URL") + "/run";

    public TaskScheduler(final WorkspacesRepository workspaces, final UsersRepository users) {
        this.workspaces = workspaces;
        this.users = users;
    }

    private void createTasks() {

        List<Workspace> ws = this.workspaces.findAll();

        for (Workspace w : ws) {
            this.taskIdentifier = w.generateTasks(this.taskIdentifier);
            w.schedule();
            w.updateStatus();
            this.workspaces.save(w);
        }
    }

    public void cleanTaskAssignments() {

        List<User> usrs = this.users.findAll();

        for (User u : usrs) {
            u.cleanTaskAssignments();
            this.users.save(u);
        }
    }

    private void assignTasks() {

        try {

            System.out.println(DELEGATOR);
            HttpResponse response = Request.Get(DELEGATOR).execute().returnResponse();

            int statusCode = response.getStatusLine().getStatusCode();

            if (statusCode == 200) {
                log.info("Tasks were sucessfully scheduled.");
            } else {
                log.error("Something went wrong while scheduling the tasks");
            }

        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }

    public void schedule() {
        this.createTasks();
        this.cleanTaskAssignments();
        this.assignTasks();
    }
}
