!/usr/bin/env bash

export API_URL="http://localhost:8080"

# GET Requests MainController

function scheduler() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X GET "$API_URL/scheduler"
}

function get_workspace_admin() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X GET "$API_URL/workspaces/$2/admin"
}

function get_workspace_users() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X GET "$API_URL/workspaceusers/$2"
}

function get_user_workspaces() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X GET "$API_URL/userworkspaces/$2"
}

function get_user_schedule() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X GET "$API_URL/schedule/$2"
}

function get_user_skills_in_workspace() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X GET "$API_URL/workspaces/$2/skills/$3"
}

# GET Requests UsersController

function send_recover_password_email() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X GET "$API_URL/users/$2/recover"
}

function get_all_users() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X GET "$API_URL/users/all"
}

function get_user() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X GET "$API_URL/users/$2"
}

function get_user_workspace_invites() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X GET "$API_URL/users/$2/invites"
}

# GET Requests WorkspacesController

function get_workspaces() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X GET "$API_URL/workspaces"
}

function get_workspace() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X GET "$API_URL/workspaces/$2"
}

function get_workspace_skills() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X GET "$API_URL/workspaces/$2/skills"
}

function get_workspace_task() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X GET "$API_URL/workspaces/$2/tasks/$3"
}

function get_workspace_schedule() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X GET "$API_URL/workspaces/$2/schedule"
}

function get_workspace_tasks() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: aaplication/json" \
    -X GET "$API_URL/workspaces/$2/tasks"
}

function get_workspace_reports() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X GET "$API_URL/workspaces/$2/reports"
}

# POST Requests UsersController

function signup() {
  curl -sS -H "Content-Type: application/json" \
    -X POST "$API_URL/users/signup" -d "{
        \"username\": \"$1\",
        \"name\": \"$2\",
        \"email\": \"$3\",
        \"password\": \"$4\",
        \"avatar\": \"$5\"
  }"
}

function login() {
  curl -sS -H "Content-Type: application/json" \
    -X POST "$API_URL/users/signin" -d "{
        \"username\": \"$1\",
        \"password\": \"$2\"
    }"
}

function add_task() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X POST "$API_URL/users/$2/workspaces/$3/tasks/$4"
}

function invite_to_workspace() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X POST "$API_URL/users/$2/invites/$3"
}

# POST Requests WorkspacesController

function add_taskform() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X POST "$API_URL/workspaces/$2/tasks" -d "{
	\"id\": \"$3\",
	\"title\": \"$4\",
	\"description\": \"$5\",
	\"workspace\": \"$2\",
	\"workload\": $6,
	\"flexible\": $7,
	\"skillset\": \"$8\",
	\"freq\": \"$9\"
  }"
}

function add_workspace() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X POST "$API_URL/workspaces" -d "{
        \"name\": \"$2\",
        \"adminId\": \"$3\",
        \"image\": \"$4\"
    }"
}

function add_skill_to_workspace() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X POST "$API_URL/workspaces/$2/skills/$3"
}

function add_userskill_to_workspace() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X POST "$API_URL/workspaces/$2/users/$3/skills/$4"
}

function add_task_to_workspace() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X POST "$API_URL/workspaces/$2/tasks" -d "{
        \"id\": \"$3\",
        \"title\": \"$4\",
        \"description\": \"$5\",
        \"assignee\": \"$6\",
        \"workspace\": \"$7\",
        \"workload\": $8,
        \"status\": \"$9\",
        \"flexible\": ${10},
        \"skillset\": \"${11}\",
        \"expiringDayOfWeek\": ${12},
        \"freq\": \"${13}\"
    }"
}

function update_workspace_user_workload() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X POST "$API_URL/workspaces/$2/users/$3/$4"
}

function submit_report() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X POST "$API_URL/workspaces/$2/report" -d "{
        \"reporterId\": \"$3\",
	\"reportedId\": \"$4\",
	\"taskId\": \"$5\",
	\"taskDate\": \"$6\",
	\"description\": \"$7\"
    }"
}

# POST Requests MainController

function sign_user_to_workspace() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X POST "$API_URL/workspaces/$2/users/$3"
}

# PUT Requests UsersController

function update_user() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X PUT "$API_URL/users/$2" -d "{
        \"username\": \"$3\",
        \"name\": \"$4\",
        \"email\": \"$5\",
        \"password\": \"$6\",
        \"avatar\": \"$7\",
        \"currentPassword\": \"$8\"
    }"
}

function recover_password() {
  curl -sS -H "Content-Type: application/json" \
    -X POST "$API_URL/users/recover/$1" -d "{
        \"password\": \"$2\"
    }"
}

function add_user_to_workspace() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X PUT "$API_URL/users/$2/workspaces/$3"
}

function update_workload() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X PUT "$API_URL/users/$2/workspaces/$3/$4"
}

# PUT Requests WorkspacesController

function complete_task() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X PUT "$API_URL/workspaces/$2/tasks/$3/status"
}

function update_workspace() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X PUT "$API_URL/workspaces/$2" -d "{
        \"name\": \"$3\",
        \"image\": \"$4\"
    }"
}

function add_user_to_workspace() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X PUT "$API_URL/workspaces/$2/users/$3"
}

# DELETE Requests UsersController

function delete_user() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X DELETE "$API_URL/users/$2"
}

function remove_user_from_workspace() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X DELETE "$API_URL/users/$2/workspaces/$3"
}

function remove_user_task() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X DELETE "$API_URL/users/$2/workspaces/$3/$4"
}

# DELETE Requests WorkspacesController

function remove_workspace() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X DELETE "$API_URL/workspaces/$2"
}

function remove_user_from_workspace() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X DELETE "$API_URL/workspaces/$2/users/$3"
}

function remove_skill_from_workspace() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X DELETE "$API_URL/worksapces/$2/skills/$3"
}

function remove_user_skill_from_workspace() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X DELETE "$API_URL/workspaces/$2/users/$3/skills/$4"
}

function remove_task_from_workspace() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X DELETE "$API_URL/workspaces/$2/tasks/$3"
}

# DELETE Requests MainController

function remove_user_from_workspace() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X DELETE "$API_URL/workspaces/$2/users/$3"
}

function remove_user_skill() {
  curl -sS -H "Authorization: Bearer $1" -H "Content-Type: application/json" \
    -X DELETE "$API_URL/workspaces/$2/users/$3/skills/$4"
}
