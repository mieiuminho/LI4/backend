#!/usr/bin/env bash

. ../../../../scripts/helpers.sh
. ../functions.sh

user="${1:-pedro}"
password="${2:-delegatewise}"
workspace="${3:-5e90dd7cdb59d26fd0cdcb6a}"

echo_info "Authenticating with $user"
jwt="$(login "$user" "$password" | jq .jwt | sed 's/"//g')"
echo_done "Authenticated with $user"

echo_info "Testing GET workspaces/{$workspace}/admin"
get_workspace_admin "$jwt" "$workspace" | jq
