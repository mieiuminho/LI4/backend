#!/usr/bin/env bash

. ../../../../scripts/helpers.sh
. ../functions.sh

user="${1:-pedro}"
password="${2:-delegatewise}"
skill="${3:-mopping}"
workspace="${4:-5e90dd7cdb59d26fd0cdcb6c}"

echo_info "Authenticating with $user"
jwt="$(login "$user" "$password" | jq .jwt | sed 's/"//g')"
echo_done "Authenticated with $user"

echo_info "Testing GET /workspaces/${workspace}/users/{$user}/skills/{$skill}"
remove_user_skill "$jwt" "$workspace" "$user" "$skill" | jq
