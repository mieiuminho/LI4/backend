#!/usr/bin/env bash

MODULES=(
  "workspaces"
  "users"
  "main"
)

function run_all() {
  for script in *.sh; do
    bash "$script"
  done
}

for module in "${MODULES[@]}"; do
  echo "$module"
  cd "$module" || exit 1
  run_all
  cd .. || exit 2
  echo
done
