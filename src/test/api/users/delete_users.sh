#!/bin/bash

. ../../../../scripts/helpers.sh
. ../functions.sh

user="${1:-nelson}"
password="${2:-delegatewise}"
echo_info "Authenticating with $user"
jwt="$(login "$user" "$password" | jq .jwt | sed 's/"//g')"
echo_done "Authenticated with $user"

echo_info "Testing DELETE users/{$user}"
delete_user "$jwt" "$user" | jq
