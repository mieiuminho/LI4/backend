#!/usr/bin/env bash

. ../../../../scripts/helpers.sh
. ../functions.sh

avatar_file=$(cat example_image)

user="${1:-joao}"
name="${2:-Joao Ribeiro}"
email="${3:-joaoribeiro@delegatewise.com}"
password="${4:-delegatewise}"
avatar="${5:-$avatar_file}"

echo_info "Testing POST users/{$user}"
signup "$user" "$name" "$email" "$password" "$avatar" | jq
