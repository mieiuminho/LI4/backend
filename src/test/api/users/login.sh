#!/usr/bin/env bash

. ../../../../scripts/helpers.sh
. ../functions.sh

user="${1:-pedro}"
password="${2:-delegatewise}"

echo_info "Testing POST users/signin"
login "$user" "$password" | jq
