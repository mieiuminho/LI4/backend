#!/usr/bin/env bash

. ../../../../scripts/helpers.sh
. ../functions.sh

token="${1:-token}"
password="${2:-cenitas}"

echo_info "Testing POST users/recover/{$token}"
recover_password "$token" "$password" | jq
