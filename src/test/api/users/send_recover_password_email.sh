#!/usr/bin/env bash

. ../../../../scripts/helpers.sh
. ../functions.sh

user="${1:-nelsonmestevao@gmail.com}"
password="${2:-delegatewise}"

echo_info "Authenticating with $user"
jwt="$(login "$user" "$password" | jq .jwt | sed 's/"//g')"
echo_done "Authenticated with $user"

echo_info "Testing GET users/{$user}/invites"
send_recover_password_email "$jwt" "$user" | jq
