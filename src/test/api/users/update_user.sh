#!/usr/bin/env bash

. ../../../../scripts/helpers.sh
. ../functions.sh

avatar_file=$(cat example_image)

user="${1:-hugo}"
password="${2:-delegatewise}"
new_username="${3:-huguinho}"
name="${4:-Huguinho Carvalho}"
email="${5:-huguinhocarvalho@delegatewise.com}"
new_password="${6:-delegatewise}"
avatar="${7:-$avatar_file}"
currentPassword="${8:-delegatewise}"

echo_info "Authenticating with $user"
jwt="$(login "$user" "$password" | jq .jwt | sed 's/"//g')"
echo_done "Authenticated with $user"

echo_info "Testing PUT users/{$user}"
update_user "$jwt" "$user" "$new_username" "$name" "$email" "$new_password" "$avatar" "$currentPassword" | jq
