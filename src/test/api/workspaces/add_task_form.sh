#!/usr/bin/env bash

. ../../../../scripts/helpers.sh
. ../functions.sh

user="${1:-pedro}"
password="${2:-delegatewise}"
# University Office
id="${3:-null}"
title="${4:-Tarefa de teste}"
description="${5:-É um teste, a sério}"
workspace="${6:-5e90dd7cdb59d26fd0cdcb69}"
workload="${7:-2.0}"
flexible="${8:-true}"
skillset="${9:-javascript}"
freq="${10:-ONE_OFF}"

echo_info "Authenticating with $user"
jwt="$(login "$user" "$password" | jq .jwt | sed 's/"//g')"
echo_done "Authenticated with $user"

echo_info "Testing POST /workspaces/{$workspace}/tasks"
add_taskform "$jwt" "$workspace" "$id" "$title" "$description" "$workload" "$flexible" "$skillset" "$freq" | jq
