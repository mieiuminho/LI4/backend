#!/usr/bin/env bash

. ../../../../scripts/helpers.sh
. ../functions.sh

image_example=$(cat example_image)
user="${1:-pedro}"
password="${2:-delegatewise}"
name="${3:-WorkspaceDeTeste}"
adminId="${4:-5e90dd7cdb59d26fd0cdcb66}"
image="${5:-$image_example}"

echo_info "Authenticating with $user"
jwt="$(login "$user" "$password" | jq .jwt | sed 's/"//g')"
echo_done "Authenticated with $user"

echo_info "Testing POST /workspaces"
add_workspace "$jwt" "$name" "$adminId" "$image" | jq
