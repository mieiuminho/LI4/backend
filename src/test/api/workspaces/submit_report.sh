#!/usr/bin/env bash

. ../../../../scripts/helpers.sh
. ../functions.sh

user="${1:-pedro}"
password="${2:-delegatewise}"
# University Office
workspace="${3:-5e90dd7cdb59d26fd0cdcb69}"
reporterId="${4:-5e90dd7cdb59d26fd0cdcb64}"
reportedId="${5:-5e90dd7cdb59d26fd0cdcb65}"
taskId="${6:-0}"
taskDate="${7:-Thu Jun 18 20:56:02 EDT 2009}"
description="${8:-descricao_teste}"

echo_info "Authenticating with $user"
jwt="$(login "$user" "$password" | jq .jwt | sed 's/"//g')"
echo_done "Authenticated with $user"

echo_info "Testing GET /workspaces/{$workspace}/skills"
submit_report "$jwt" "$workspace" "$reporterId" "$reportedId" "$taskId" "$taskDate" "$description" | jq
