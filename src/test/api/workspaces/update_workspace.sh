#!/usr/bin/env bash

. ../../../../scripts/helpers.sh
. ../functions.sh

example_image="$(cat example_image)"
user="${1:-pedro}"
password="${2:-delegatewise}"
workspace="${3:-5e90dd7cdb59d26fd0cdcb6c}"
name="${4:-NewName}"
image="${5:-${example_image}}"

echo_info "Authenticating with $user"
jwt="$(login "$user" "$password" | jq .jwt | sed 's/"//g')"
echo_done "Authenticated with $user"

echo_info "Testing PUT /workspaces/${workspace}"
update_workspace "$jwt" "$workspace" "$name" "$image" | jq
