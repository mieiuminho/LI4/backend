package delegatewise.model;

import static org.junit.Assert.*;

import org.junit.Test;

public class UserTest {
    private User user =
            new User("nelsonmestevao", "Nelson", "nelson@uminho.pt", "12345678", "avatar");

    @Test
    public void getName() {
        assertTrue("Nelson".equals(user.getName()));
    }
}
