package delegatewise.model;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class WorkspaceTest {

    private Workspace workspace = new Workspace("uminho", "nelsonmestevao", "icon");

    @Test
    public void getName() {
        assertTrue("uminho".equals(workspace.getName()));
    }
}
